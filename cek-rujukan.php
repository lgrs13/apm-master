<?php
include ('config.php');
if(!empty($_POST['no_peserta'])){


    $no_peserta = $_POST['no_peserta'];
    $selectBPJS = fetch_array(query("select no_peserta from pasien where no_rkm_medis = '$no_peserta'"));
    if ($selectBPJS['no_peserta']!='' && $selectBPJS['no_peserta'] != '-') {
        $no_peserta = $selectBPJS['no_peserta'];
    }

    //ambli data dari database databse
    $type = "BPJS";    
    $getAPI = fetch_array(query("select * from data_webservice where ID = '$type'"));
    $url = $getAPI['base_url'].'/Rujukan/Peserta/'.$no_peserta;

    date_default_timezone_set('UTC');
    $tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
    $signature = hash_hmac('sha256', $getAPI['user_key']."&".$tStamp, $getAPI['secret_key'], true);
    $encodedSignature = base64_encode($signature);

    $uptade = (query("update data_webservice set token='$encodedSignature' where ID = '$type'"));

    // echo "X-cons-id: " .$getAPI['user_key'];
    // echo "X-timestamp:" .$tStamp;
    // echo "X-signature: " .$encodedSignature;

    $headers = array();
    $headers[] = "X-cons-id: " .$getAPI['user_key'];
    $headers[] = "X-timestamp:" .$tStamp;
    $headers[] = "X-signature: " .$encodedSignature;
    $headers[] = "Content-Type: application/json; charset=utf-8";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $return = curl_exec($ch);
    curl_close($ch);

    $respon = json_decode($return, true);
        if ($respon['metaData']['code']==200){
            // saveUser($respon);
        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date("Y-m-d");
            $tentukan_hari = date('D',strtotime($tanggal));
            $day = array(
                'Sun' => 'AKHAD',
                'Mon' => 'SENIN',
                'Tue' => 'SELASA',
                'Wed' => 'RABU',
                'Thu' => 'KAMIS',
                'Fri' => 'JUMAT',
                'Sat' => 'SABTU'
            );
            $hari=$day[$tentukan_hari];
            $query2 = $db->query("
            SELECT
            a.kd_poli,
            b.nm_poli,
            c.kd_poli_bpjs,
            c.nm_poli_bpjs
            FROM jadwal a
            INNER JOIN poliklinik b ON a.kd_poli = b.kd_poli
            INNER JOIN maping_poli_bpjs c ON a.kd_poli = c.kd_poli_rs
            WHERE a.hari_kerja LIKE '%$hari%'
            GROUP BY
                a.kd_poli
            ");
        if($respon['response']['rujukan']['peserta']['statusPeserta']['keterangan']=='AKTIF'){
            $dokterDPJP = get_dokter($respon['response']['rujukan']['poliRujukan']['kode']);
            $data['status'] = 'ok';
            $data['result'] = $respon;
            $data['dokter_awal'] = $dokterDPJP;
            while ($poli = $query2->fetch_assoc()) {
                $data['poli'][] = $poli;
            }
            $data['kunjungan_ke'] = '1';
            echo json_encode($data);
        }else {
            $data['status'] = 'err';
            $data['result'] = 'Maaf kartu BPJS Anda TIDAK AKTIF, Silahkan Hubungi petugas';
            echo json_encode($data);
        }
        // saveSEP($data);
        // echo $data['response']['rujukan']['peserta']['mr']['noMR'];
        
    }else {
        $data['status'] = 'err';
        $data['result'] = $respon['metaData']['message'];
        echo json_encode($data);
    }

}

 function get_dokter($poliBPJS){
    $tanggal=date("Y-m-d");
    $tentukan_hari=date('D',strtotime($tanggal));
        $day = array(
        'Sun' => 'AKHAD',
        'Mon' => 'SENIN',
        'Tue' => 'SELASA',
        'Wed' => 'RABU',
        'Thu' => 'KAMIS',
        'Fri' => 'JUMAT',
        'Sat' => 'SABTU'
        );
    $hari=$day[$tentukan_hari];

    $dokter=fetch_array(query("SELECT
        a.kd_dokter,
        a.kd_poli,
        b.kd_dokter_bpjs,
        c.kd_poli_bpjs,
        d.nm_dokter
        FROM
        jadwal a
        INNER JOIN maping_dokter_dpjpvclaim b on a.kd_dokter = b.kd_dokter
        INNER JOIN maping_poli_bpjs c on a.kd_poli = c.kd_poli_rs
        INNER JOIN dokter d on a.kd_dokter = d.kd_dokter
        AND c.kd_poli_bpjs = '$poliBPJS'
        AND a.hari_kerja = '$hari'
    "));

    $data_dokter = $dokter['kd_dokter_bpjs'];

    return $data_dokter;
}

function saveUser($data){

    try { 
    $insert = query("INSERT INTO pasien VALUES(
        '{$data['response']['rujukan']['peserta']['mr']['noMR']}',
        '{$data['response']['rujukan']['peserta']['nama']}',
        '{$data['response']['rujukan']['peserta']['nik']}',
        '{$data['response']['rujukan']['peserta']['sex']}',
        '-',
        '{$data['response']['rujukan']['peserta']['tglLahir']}',
        '-',
        '-',
        '-',
        '{$data['response']['rujukan']['peserta']['jenisPeserta']['keterangan']}',
        'BELUM MENIKAH',
        '-',
        '0000-00-00',
        '{$data['response']['rujukan']['peserta']['mr']['noTelepon']}',
        '0 Th 0 Bl 0 Hr',
        '-',
        '-',
        '-',
        'A65',
        '{$data['response']['rujukan']['peserta']['noKartu']}', 
        1,
        1,
        1,
        '-',
        'ALAMAT',
        'KELURAHAN',
        'KECAMATAN',
        'KABUPATEN',
        0,
        0,
        0,
        '0',
        '-',
        '-',
        '0',
        '-'
        )");
        return true;
    }catch (\Throwable $th) {
        return false;
    }
}

?>
