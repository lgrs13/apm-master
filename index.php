<?php
include ('config.php');
session_start();
?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="fontawesome-free-5.6.3-web/css/all.css">
  <link href="css/gijgo.min.css" rel="stylesheet" type="text/css" />

  <style>
    .modal-full {
      min-width: 100%;
      margin: 0;
    }

    .modal-full .modal-content {
      min-height: 90vh;
      border-radius: 0px;
      box-shadow: 0 0 20px 8px rgba(0, 0, 0, 0.7);
    }

    .modal-fix {
      min-width: 1024px;
      margin: 0;
    }

    .modal-fix .modal-content {
      min-height: 100vh;
    }

    .modal .tab-content {
      min-height: 50vh;
    }

    .nav-pills.nav-wizard>li {
      position: relative;
      overflow: visible;
      border-right: 8px solid transparent;
      border-left: 8px solid transparent;
    }

    .nav-pills.nav-wizard>li+li {
      margin-left: 0;
    }

    .nav-pills.nav-wizard>li:first-child {
      border-left: 0;
    }

    .nav-pills.nav-wizard>li:first-child a {
      border-radius: 5px 0 0 5px;
    }

    .nav-pills.nav-wizard>li:last-child {
      border-right: 0;
    }

    .nav-pills.nav-wizard>li:last-child a {
      border-radius: 0 5px 5px 0;
    }

    .nav-pills.nav-wizard>li a {
      border-radius: 0;
      background-color: #eee;
    }

    .nav-pills.nav-wizard>li:not(:last-child) a:after {
      position: absolute;
      content: "";
      top: 0px;
      right: -20px;
      width: 0px;
      height: 0px;
      border-style: solid;
      border-width: 20px 0 20px 20px;
      border-color: transparent transparent transparent #eee;
      z-index: 150;
    }

    .nav-pills.nav-wizard>li:not(:first-child) a:before {
      position: absolute;
      content: "";
      top: 0px;
      left: -20px;
      width: 0px;
      height: 0px;
      border-style: solid;
      border-width: 20px 0 20px 20px;
      border-color: #eee #eee #eee transparent;
      z-index: 150;
    }

    .nav-pills.nav-wizard>li:hover:not(:last-child) a:after {
      border-color: transparent transparent transparent #aaa;
    }

    .nav-pills.nav-wizard>li:hover:not(:first-child) a:before {
      border-color: #aaa #aaa #aaa transparent;
    }

    .nav-pills.nav-wizard>li:hover a {
      background-color: #aaa;
      color: #fff;
    }

    .nav-pills.nav-wizard>li:not(:last-child) a.active:after {
      border-color: transparent transparent transparent #428bca;
    }

    .nav-pills.nav-wizard>li:not(:first-child) a.active:before {
      border-color: #428bca #428bca #428bca transparent;
    }

    .nav-pills.nav-wizard>li a.active {
      background-color: #428bca;
    }

    

/** SPINNER CREATION **/

.loader {
  position: relative;
  text-align: center;
  margin: 15px auto 35px auto;
  z-index: 9999;
  display: block;
  width: 80px;
  height: 80px;
  border: 10px solid #ffc107;
  border-radius: 50%;
  border-top-color: #1e7e34;
  animation: spin 1s ease-in-out infinite;
  -webkit-animation: spin 1s ease-in-out infinite;
}

@keyframes spin {
  to {
    -webkit-transform: rotate(360deg);
  }
}

@-webkit-keyframes spin {
  to {
    -webkit-transform: rotate(360deg);
  }
}


.modal-backdrop.show {
  opacity: 0.75;
}




    /* .datepicker,
    .table-condensed {
      width: 500px;
      height: 500px;
    } */
  </style>
  <title>ANJUNGAN MANDIRI</title>
</head>

<body>
  <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-6">Anjungan Pasien Mandiri Pelayanan Rawat Jalan</h1>
    <h2 class="display-5"><?php echo $dataSettings['nama_instansi']; ?></h2>
  </div>
  <br><br>
  <div class="container">
    <div class="card-deck mb-3 text-center">
      <div class="card mb-4 shadow-sm" data-toggle="modal" data-target="#daftar_poli">
            <div class="card-body btn btn-lg btn-success">
              <ul class="list-unstyled mt-3 mb-4">
                <span style="font-size: 120px; color: white;"><i class="fas fa-stethoscope"></i></span>
              </ul>
              <p style="font-size: 1.5rem;"> PENDAFTARAN POLI</p>
            </div>
          </div>
      <div class="card mb-4 shadow-sm" data-toggle="modal" data-target="#daftar_penjadwalan">
        <div class="card-body btn btn-lg btn-primary">
          <ul class="list-unstyled mt-3 mb-4">
            <span style="font-size: 120px; color: white;"><i class="fas fa-user-md"></i></span>
          </ul>
          <p style="font-size: 1.5rem;"> DAFTAR KONTROL ULANG </p>
        </div>
      </div>
      <div class="card mb-4 shadow-sm" data-toggle="modal" data-target="#daftar_baru">
        <div class="card-body btn btn-lg btn-warning">
        <ul class="list-unstyled mt-3 mb-4">
            <span style="font-size: 120px; color: white;"><i class="fas fa-address-book"></i></span>
            </ul>
            <p style="font-size: 1.5rem; "> PENDAFTARAN PASIEN BARU </p>
        </div>
      </div>
    </div>
  </div>
  <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center text-danger">
    <h3 class="display-6">Silahkan hubungi petugas jika anda mengalami kesulitan.</h3>
  </div>

  <!-- Modal pendaftaran poli -->
  <div class="modal fade" id="daftar_poli" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-fix" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Penjadwalan Pasien Mandiri</h5>
          <button type="button" class="close" data-dismiss="modal"
            onclick="javascript:window.location='<?php echo URL; ?>'" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <ul class="nav nav-tabs mt-3 pl-2 pr-2">
          <!-- -----SUB BPJS---- -->
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#poli_bpjs">BPJS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#poli_umum">Umum Dan Penunjang</a>
          </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          
          <!-- ------------------ -->
          <!-- ---section bpjs--- -->
          <!-- ------------------ -->
          <div class="tab-pane container active" id="poli_bpjs">
            <div class="modal-body">
              <ul class="nav nav-pills nav-wizard" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#infonorawat" role="tab">No. BPJS</a>
                <li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#reviewsep" role="tab">Review Rujukan</a>
                <li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#reviewCetak" role="tab">Cetak</a>
                <li>
              </ul>
              <div class="tab-content mt-2">
                <div class="tab-pane fade show active" id="infonorawat" role="tabpanel">
                  <h4>Data Pendaftaran</h4>
                  <div class="form-group">
                    <label for="campaignName">Masukan Nomor Indentitas Anda seperti No. rekam medis atau NO. BPJS</label>
                    <input type="text" class="form-text form-control form-control-lg" id="no_rawat" value="" autofocus></input>
                  </div>
                  <button class="btn btn-warning" id="cek_rujukan">Cek Nomor</button>
                </div>
                <!-- -------------------------- -->
                <!-- ----- REVIEW RUJUKAN ----- -->
                <!-- -------------------------- -->
                <div class="tab-pane fade" id="reviewsep" role="tabpanel">
                  <section class="content">
                    <div class="container-fluid">
                      <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="card">
                            <div class="body">
                              <div class="container" style="margin-top: 30px; margin-bottom: 20px;" action="cetak">
                                <div class="row clearfix">
                                  <div class="col-md-4">
                                    <div class="form-group">
                                      <img src="img/bpjslogo.png" height="60" width="350">
                                    </div>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    Nomer Rujukan
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_no_rujukan"></span>
                                  </div>
                                
                                  <div class="col-md-2">
                                    Tgl rujukan 
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_tgl_rujukan"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    No. Kartu
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_no_kartu"></span>
                                  </div>
                                  <div class="col-md-2">
                                    Jenis Kelamin
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_jkel"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    Nama Peserta
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_nama_pasien"></span>
                                  </div>
                                  <div class="col-md-2">
                                    No. Rawat
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_no_rawat"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    Tgl. Lahir
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_tanggal_lahir"></span>
                                  </div>
                                  <div class="col-md-2">
                                    Jenis Peserta
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_jenis_peserta"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    No.Telepon
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_notelep"></span>
                                  </div>
                                  <div class="col-md-2">
                                    MR
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_peserta"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                <div class="col-md-2">
                                    Faskes Perujuk
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_kdppkrujukan"></span> - <span id="sep_nmppkrujukan"></span>
                                  </div>
                                  <div class="col-md-2">
                                    COB
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_cob"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                <div class="col-md-2">
                                    Diagnosa Awal
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_kodeicd"></span> - <span id="sep_nmdiagnosaawal"></span>
                                  </div>
                                  <div class="col-md-2">
                                    Jenis Rawat
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="sep_kode_pelayanan"></span> - <span id="sep_nmpelayanan"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                <div class="col-md-2">
                                    Catatan
                                  </div>
                                  <div class="col-md-10">
                                    : <span id="sep_catatan"></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>

                  <div class="form-row row" style="margin-left: 10px; margin-right: 10px;">
                    <div class="col form-group">
                      <label>Poli yang dituju</label>
                      <select id="sep_nmpolitujuan" class="form-control">
                        <option value="">Pilih Poli</option>
                      </select>
                    </div>
                    <div class="col form-group">
                      <label>Dokter yang tejadwal</label>
                      <select id="sep_doktertujuan" class="form-control">
                        <option value="">Pilih Dokter</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group" hidden>
                    <label>dokter DPJP</label>
                    <input class="form-control" placeholder="" id="dokter_dpjp_awal"></input>
                  </div>
                  <br>
                  <button style="margin-left:10px;" class="btn btn-secondary" id="simpan_sep">Simpan SEP</button>
                </div>

                <!-- -------------------- -->
                <!-- ----- CETAK SEP----- -->
                <!-- -------------------- -->
                <div class="tab-pane fade" id="reviewCetak" role="tabpanel">
                  <section class="content">
                    <div class="container-fluid">
                      <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="card">
                            <div class="body">
                              <div class="container" style="margin-top : 50px;" action="cetak">
                                <div class="row clearfix">
                                  <div class="col-md-4">
                                    <div class="form-group">
                                      <img src="img/bpjslogo.png" height="50" width="300">
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group">
                                      <h6>
                                        SURAT ELEGIBILITAS PESERTA
                                      </h6>
                                      <h6>
                                        <?php echo $dataSettings['nama_instansi']; ?>
                                      </h6>
                                    </div>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    No. SEP
                                  </div>
                                  <div class="col-md-10">
                                    : <span id="review_sep_no_sep"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    Tgl. SEP
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_tglsep"></span>
                                  </div>
                                  <div class="col-md-2">
                                    Peserta
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_peserta"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    No. Kartu
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_no_kartu"></span> (MR. <span id="review_sep_no_mr"></span> )
                                  </div>
                                  <div class="col-md-2">
                                    COB
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_cob"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    Nama Peserta
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_nama_pasien"></span>
                                  </div>
                                  <div class="col-md-2">
                                    Jns. Rawat
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_jenis_rawat"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    Tgl. Lahir
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_tanggal_lahir"></span> Kelamin : <span id="review_sep_jk"></span> 
                                  </div>
                                  <div class="col-md-2">
                                    Kls. Rawat
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_klsrawat"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    No.Telepon
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_notelep"></span>
                                  </div>
                                  <div class="col-md-2">
                                    No.Rawat
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_norawat"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    Sub/Spesialis
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_nmpolitujuan"></span>
                                  </div>
                                  <div class="col-md-2">
                                    Penjamin
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_penjamin"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    Faskes Perujuk
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_nmppkrujukan"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    Diagnosa Awal
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_nmdiagnosaawal"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  
                                  
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    Catatan
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="review_sep_catatan"></span>
                                  </div>
                                  <div class="col-md-6" style="left: 90px;">
                                    Pasien/Keluarga Pasien
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-6">
                                    <small><em>
                                        <font size="1px">*Saya Menyetujui BPJS Kesehatan menggunakan informasi Medis
                                          Pasien jika diperlukan.</font>
                                      </em></small>
                                  </div>
                                  <div class="col-md-6">
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-6">
                                  </div>
                                  <div class="col-md-6">
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-6">
                                    <small><em>
                                        <font size="1px">**SEP bukan sebagai bukti penjaminan peserta</font>
                                      </em></small>
                                  </div>
                                  <div class="col-md-6">
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-2">
                                    <small><em>
                                        <font size="1px">Cetakan ke 1</font>
                                      </em></small>
                                  </div>
                                  <div class="col-md-10">
                                    <small><em>
                                        <font size="1px"><?php echo date("d/m/Y h:i:s A");?></font>
                                      </em></small>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-md-4">
                                  </div>
                                  <div class="col-md-8">
                                    <center>----------------------------------------------------</center>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <br>

                  <section class="content" id="printSEP" style="display: none; width: 100px">
                    <link rel="stylesheet" href="css/bootstrap.min.css"
                      integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
                      crossorigin="anonymous">
                    <div class="container-fluid">
                      <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="card">
                            <div class="body">
                            <div class="container" style="margin-top : 50px;" action="cetak">
                                <div class="row clearfix">
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                      <img src="img/bpjslogo.png" height="50" width="300">
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                      <h6>
                                        SURAT ELEGIBILITAS PESERTA
                                      </h6>
                                      <h6>
                                        <?php echo $dataSettings['nama_instansi']; ?>
                                      </h6>
                                    </div>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-2">
                                    No. SEP
                                  </div>
                                  <div class="col-md-10">
                                    : <span id="cetak_sep_no_sep"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-2">
                                    Tgl. SEP
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_tglsep"></span>
                                  </div>
                                  <div class="col-sm-2">
                                    Peserta
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_peserta"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-2">
                                    No. Kartu
                                  </div>
                                  <div class="col-md-4">
                                    : <span id="cetak_sep_no_kartu"></span> (MR. <span id="cetak_sep_no_mr"></span> )
                                  </div>
                                  <div class="col-sm-2">
                                    COB
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_cob"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-2">
                                    Nama Peserta
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_nama_pasien"></span>
                                  </div>
                                  <div class="col-sm-2">
                                    Jns. Rawat
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_jenis_rawat"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-2">
                                    Tgl. Lahir
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_tanggal_lahir"></span> Kelamin : <span id="cetak_sep_jk"></span> 
                                  </div>
                                  <div class="col-sm-2">
                                    Kls. Rawat
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_klsrawat"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-2">
                                    No.Telepon
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_notelep"></span>
                                  </div>
                                  <div class="col-sm-2">
                                    No.Rawat
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_norawat"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-2">
                                    Sub/Spesialis
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_nmpolitujuan"></span>
                                  </div>
                                  <div class="col-sm-2">
                                    Penjamin
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_penjamin"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-2">
                                    Faskes Perujuk
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_nmppkrujukan"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-2">
                                    Diagnosa Awal
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_nmdiagnosaawal"></span>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  
                                  
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-2">
                                    Catatan
                                  </div>
                                  <div class="col-sm-4">
                                    : <span id="cetak_sep_catatan"></span>
                                  </div>
                                  <div class="col-sm-6" style="left: 90px;">
                                    Pasien/Keluarga Pasien
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-6">
                                    <small><em>
                                        <font size="1px">*Saya Menyetujui BPJS Kesehatan menggunakan informasi Medis
                                          Pasien jika diperlukan.</font>
                                      </em></small>
                                  </div>
                                  <div class="col-sm-6">
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-6">
                                  </div>
                                  <div class="col-sm-6">
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-6">
                                    <small><em>
                                        <font size="1px">**SEP bukan sebagai bukti penjaminan peserta</font>
                                      </em></small>
                                  </div>
                                  <div class="col-sm-6">
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-2">
                                    <small><em>
                                        <font size="1px">Cetakan ke 1</font>
                                      </em></small>
                                  </div>
                                  <div class="col-sm-10">
                                    <small><em>
                                        <font size="1px"><?php echo date("d/m/Y h:i:s A");?></font>
                                      </em></small>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-4">
                                  </div>
                                  <div class="col-sm-8">
                                    <center>----------------------------------------------------</center>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <!-- <center><button id="cetakPosBPJS" style="width:200px;" class="btn btn-primary btn-block"
                      onclick="printSEP();">Cetak</button></center> -->

                      <!-- <button class="btn btn-primary btn-block" id="cetakPosBPJS">Cetak</button> -->
                      
                      <!-- <form action="print-SEP.php" method="post">
                      <input type="hidden" name="no_sep" value="0114R0530220V002200" />
                    </form>  -->
                      <button style="width: 150px;" class="btn btn-primary btn-block" id="cetakPosBPJS" >Cetak</button>
                      
                </div>
              </div>
              <div class="progress mt-5">
                <div class="progress-bar" role="progressbar" style="width: 35%" aria-valuenow="25" aria-valuemin="0"
                  aria-valuemax="100">Lanjut 1 dari 3</div>
              </div>
            </div>

          </div>
          <!-- ------------------ -->
          <!-- ---section POLI--- -->
          <!-- ------------------ -->
          <div class="tab-pane container fade " id="poli_umum">
            <div class="modal-body">
              <ul class="nav nav-pills nav-wizard" id="myTab" role="tablist">
                <li class="nav nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#info_rm" role="tab">No. RM</a>
                <li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#poli_daftar" role="tab">Pilih Poli dan Dokter</a>
                <li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#reviewPrintDaftarPoli" role="tab">Cetak</a>
                <li>
              </ul>
              <div class="tab-content mt-2">
                <div class="tab-pane fade show active" id="info_rm" role="tabpanel">
                  <h4>Data Diri</h4>
                  <div class="form-group">
                    <label for="campaignName">Masukan nomor rekam medik anda / kode booking anda</label>
                    <input type="text" class="form-text form-control form-control-lg" id="no_rm_poli" value=""></input>
                  </div>
                  <button class="btn btn-warning" id="cek_rm" autofocus>Cek Kartu</button>
                </div>
                <div class="tab-pane fade" id="poli_daftar" role="tabpanel">
                  <h4>Informasi Pribadi, Klinik dan Dokter Tujuan</h4>
                  <div class="form-row">
                    <div class="col form-group">
                      <label>Nomor Rekam Medik</label>
                      <input type="text" name="no_rkm_medis_poli" class="form-control" placeholder=""
                        id="no_rkm_medis_poli" disabled>
                    </div>
                    <div class="col form-group">
                      <label>Nama Lengkap</label>
                      <input type="text" name="nm_pasien_poli" class="form-control" placeholder=" "
                        id="nm_pasien_poli" disabled>
                    </div>
                  </div>
                  <div class="form-row">
                  <div class="col form-group">
                      <label>Kode Poli</label>
                      <select id="pilih_poli_tersedia" class="form-control">
                        <option value="">Pilih Poli</option>
                      </select>
                    </div>
                    <div class="col form-group">
                      <label>Kode Dokter</label>
                      <select id="pilih_dokter_poli" class="form-control">
                        <option value="">Pilih Dokter</option>
                      </select>
                    </div>
                    <div class="col form-group">
                      <label>Cara Bayar</label>
                      <select id="pilih_carabayar_daftar_poli" class="form-control">
                        <option value="">Pilih Cara bayar</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group" hidden>
                    <label>kd_booking</label>
                    <textarea class="form-control" placeholder="" id="kode_booking"></textarea>
                  </div>
                  <!-- <div class="row">
                    <div class="col-md-6">
                      <button class="btn btn-secondary" id="simpan_poli">Simpan Pendaftaran</button>
                    </div>
                    <div id="group_total" style="background-color: greenyellow" class="col-md-2">
                      <h1 id="txt_kuota">test</h1>
                    </div>
                    <div id="group_kuota" style="background-color: greenyellow" class="col-md-2">
                      <h1 id="txt_kuota">test</h1>
                    </div>
                  </div> -->
                  <button class="btn btn-secondary" id="simpan_poli">Simpan Pendaftaran</button>
                </div>
                <div class="tab-pane fade" id="reviewPrintDaftarPoli" role="tabpanel">
                  <h4>Cetak Bukti Daftar</h4>
                  <div style="width: 230px; font-family: Tahoma; margin: 0 auto; font-size: 11px !important;">
                    <div class="block-header">
                      <div style="font-size:12px; font-weight: bold; text-align: center;">
                        <?php echo $dataSettings['nama_instansi']; ?><br>
                        <?php echo $dataSettings['alamat_instansi']; ?><br>
                        <?php echo $dataSettings['kontak']; ?><br>
                        <hr>
                        BUKTI REGISTRASI<br>
                        ANJUNGAN PASIEN MANDIRI<br>
                      </div>
                    </div>
                    <hr>
                    <div style="width: 100px; display: inline-block;">Nama</div>: <span id="nm_pasien_rawat_cetak"></span><br>
                    <div style="width: 100px; display: inline-block;">No. RM</div>: <span
                      id="no_rkm_medis_rawat_cetak"></span><br>
                    <div style="width: 100px; display: inline-block;">poliklinik</div>: <span id="nm_poli_rawat_cetak"></span><br>
                    <div style="width: 100px; display: inline-block;">Dokter</div>: <span id="nm_dokter_rawat_cetak"></span><br>
                    <div style="width: 100px; display: inline-block;">No. Antrian Poli</div>: <span id="no_antrian_poli"
                      name="no_antrian_poli"></span><br>
                    <div style="width: 100px; display: inline-block;">no_rawat</div>: <span id="no_rawat_review"
                      name="no_rawat_review"></span><br>
                    <input type="text" name="input_no_rawat" class="form-control" id="input_no_rawat" hidden>
                    <div style="width: 100px; display: inline-block;">Tanggal Berobat</div>: <span
                      id="tgl_berobat"></span><br>
                    <div style="text-align: center;">
                      <hr>
                      Terima Kasih Atas kepercayaan Anda.<br>.<br>
                      <hr>
                      Bawalah Bukti registrasi ini ke Poliklinik yang anda tuju.<br><br>
                    </div>
                    <button class="btn btn-primary btn-block" id="cetakPOSPoli">Cetak</button>
                    <!-- <button class="btn btn-primary btn-block" onclick="printDiv('printDaftar');">Cetak</button> -->
                  </div>
                </div>
              </div>
              <div class="progress mt-5">
                <div class="progress-bar" role="progressbar" style="width: 35%" aria-valuenow="25" aria-valuemin="0"
                  aria-valuemax="100">Langkah 1 dari 3</div>
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer">
        <!-- <button class="btn btn-primary btn-block" id="cetakBuktiPendaftaran">Cetak</button> -->
          <button type="button" class="btn btn-danger" data-dismiss="modal"
            onclick="javascript:window.location='<?php echo URL; ?>'">Tutup</button>
        </div>
      </div>
    </div>     
  </div>

  <!-- Modal penjadwalan -->
  <div class="modal fade" id="daftar_penjadwalan" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-fix" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Penjadwalan Pasien Mandiri</h5>
          <button type="button" class="close" data-dismiss="modal"
            onclick="javascript:window.location='<?php echo URL; ?>'" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <ul class="nav nav-tabs mt-3 pl-2 pr-2">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#umum">Umum</a>
          </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane container active" id="umum">
            <div class="modal-body">
              <ul class="nav nav-pills nav-wizard" id="myTab" role="tablist">
                <li class="nav nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#infoPanel" role="tab">No. RM</a>
                <li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#poli" role="tab">Pilih Poli dan Dokter</a>
                <li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#reviewPrint" role="tab">Cetak</a>
                <li>
              </ul>
              <div class="tab-content mt-2">
                <div class="tab-pane fade show active" id="infoPanel" role="tabpanel">
                  <h4>Data Rekam Medik</h4>
                  <div class="form-group">
                    <label for="campaignName">Nomor Kartu Berobat</label>
                    <input type="text" class="form-text form-control form-control-lg" id="no_rm" value=""></input>
                  </div>
                  <button class="btn btn-warning" id="infoContinue">Cek Kartu</button>
                </div>
                <div class="tab-pane fade" id="poli" role="tabpanel">
                  <h4>Informasi Pribadi, Klinik dan Dokter Tujuan</h4>
                  <div class="form-row">
                    <div class="col form-group">
                      <label>Nomor Rekam Medik</label>
                      <input type="text" name="no_rkm_medis_daftar" class="form-control" placeholder=""
                        id="no_rkm_medis_daftar" disabled>
                    </div>
                    <div class="col form-group">
                      <label>Nama Lengkap</label>
                      <input type="text" name="nm_pasien_daftar" class="form-control" placeholder=" "
                        id="nm_pasien_daftar" disabled>
                    </div>
                  </div>
                  <div class="form-group" hidden>
                    <label>Alamat Lengkap</label>
                    <textarea class="form-control" placeholder="" id="alamat_daftar"></textarea>
                  </div>
                  <div class="form-row">
                    <div class="col form-group">
                      <label>Tanggal Kunjungan</label>
                      <input type="text" class="form-control" name="datepicker" id="tgl_registrasi_daftar"
                        placeholder="" disabled>
                    </div>
                    <div class="col form-group">
                      <label>Kode Poli</label>
                      <select id="pilih_poli_daftar" class="form-control">
                        <option value="">Pilih Poli</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col form-group">
                      <label>Kode Dokter</label>
                      <select id="pilih_dokter_daftar" class="form-control">
                        <option value="">Pilih Dokter</option>
                      </select>
                    </div>
                    <div class="col form-group">
                      <label>Cara Bayar</label>
                      <select id="pilih_carabayar_daftar" class="form-control">
                        <option value="">Pilih Cara bayar</option>
                      </select>
                    </div>
                  </div>
                  <!-- <button class="btn btn-secondary" id="finish">Simpan Pendaftaran</button> -->
                </div>
                <div class="tab-pane fade" id="reviewPrint" role="tabpanel">
                  <h4>Cetak Bukti Daftar</h4>
                  <div style="width: 250px; font-family: Tahoma; margin: 0 auto; font-size: 11px !important;">
                    <div class="block-header">
                      <div style="font-size:12px; font-weight: bold; text-align: center;">
                        <?php echo $dataSettings['nama_instansi']; ?><br>
                        <?php echo $dataSettings['alamat_instansi']; ?><br>
                        <?php echo $dataSettings['kontak']; ?><br>
                        <hr>
                        BUKTI PENDAFTARAN<br>
                        ANJUNGAN PASIEN MANDIRI<br>
                      </div>
                    </div>
                    <hr>
                    <div style="width: 70px; display: inline-block;">Kode booking</div>: <span id="kode_booking_cetak"
                      name="kode_booking_cetak"></span><br>
                    <input type="text" name="input_kode_booking" class="form-control" id="input_kode_booking" hidden>
                    <div style="width: 70px; display: inline-block;">Tanggal</div>: <span
                      id="tgl_registrasi_cetak"></span><br>
                    <div style="width: 70px; display: inline-block;">No. RM</div>: <span
                      id="no_rkm_medis_cetak"></span><br>
                    <div style="width: 70px; display: inline-block;">Nama</div>: <span id="nm_pasien_cetak"></span><br>
                    <div style="width: 70px; display: inline-block;">Klinik</div>: <span id="nm_poli_cetak"></span><br>
                    <div style="width: 70px; display: inline-block;">Dokter</div>: <span
                      id="nm_dokter_cetak"></span><br>

                    <div style="text-align: center;">
                      <hr>
                      Terima Kasih Atas kepercayaan Anda.<br>
                      Bawalah kartu Berobat anda dan datang 30 menit sebelumnya.<br>
                      <hr>
                      Bawalah surat rujukan atau surat kontrol asli dan tunjukkan pada petugas di Lobby
                      resepsionis.<br><br>
                    </div>

                    <div id="printDaftar" style="display: none;" class="cetak">
                      <section style="margin: 0 auto; width: 300px; font-family: Tahoma; font-size: 11px !important;">
                        <div class="block-header">
                          <div style="font-size:12px; font-weight: bold; text-align: center;">
                            <?php echo $dataSettings['nama_instansi']; ?><br>
                            <?php echo $dataSettings['alamat_instansi']; ?><br>
                            <?php echo $dataSettings['kontak']; ?><br>
                            <hr>
                            BUKTI PENDAFTARAN<br>
                            ANJUNGAN PASIEN MANDIRI<br>
                          </div>
                        </div>
                        <br>
                        <div style="width: 100px; display: inline-block;">Kode Booking </div>:<span
                          id="kode_booking_final"></span><br>
                        <div style="width: 100px; display: inline-block;">Tanggal </div>:<span
                          id="tgl_registrasi_final"></span><br>
                        <div style="width: 100px; display: inline-block;">No. Antrian </div>: <span
                          id="no_reg_final"></span><br>
                        <div style="width: 100px; display: inline-block;">Nama </div>: <span
                          id="nm_pasien_final"></span><br>
                        <div style="width: 100px; display: inline-block;">No. RM </div>: <span
                          id="no_rkm_medis_final"></span><br>
                        <div style="width: 100px; display: inline-block;">Ruang </div>: <span
                          id="nm_poli_final"></span><br>
                        <div style="width: 100px; display: inline-block;">Dokter </div>: <span
                          id="nm_dokter_final"></span><br>
                        <div style="width: 100px; display: inline-block;">Cara Bayar </div>: <span
                          id="cara_bayar_final"></span><br>
                        <div style="text-align: center;">
                          <hr>
                          Terima Kasih Atas kepercayaan Anda.<br>
                          Bawalah kartu Berobat anda dan datang 30 menit sebelumnya.<br>
                          <hr>
                          Bawalah surat rujukan atau surat kontrol asli dan tunjukkan pada petugas di Lobby
                          resepsionis.<br><br>
                        </div>
                      </section>
                    </div>
                    <button class="btn btn-primary btn-block" id="cetakPOS">Cetak</button>
                    <!-- <button class="btn btn-primary btn-block" onclick="printDiv('printDaftar');">Cetak</button> -->

                  </div>
                </div>
              </div>
              <div class="progress mt-5">
                <div class="progress-bar" role="progressbar" style="width: 35%" aria-valuenow="25" aria-valuemin="0"
                  aria-valuemax="100">Langkah 1 dari 3</div>
              </div>
            </div>

        </div>

        </div>
        <div class="modal-footer">
          <button id="finish" type="button" class="btn btn-success" disabled>SIMPAN</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"
            onclick="javascript:window.location='<?php echo URL; ?>'">Tutup</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Register -->
  <div class="modal fade" id="daftar_baru" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-fix" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Pendaftaran Pasien Mandiri</h5>
          <button type="button" class="close" data-dismiss="modal"
            onclick="javascript:window.location='<?php echo URL; ?>'" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane container active">

            <div class="modal-body">
              <ul class="nav nav-pills nav-wizard" id="myTab2" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#biodata_pasien" role="tab">Biodata Pasien sesuai
                    KTP</a>
                <li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#biodata_penunjang" role="tab">Biodata Pendamping</a>
                <li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#review_cetak_baru" role="tab">review</a>
                <li>
              </ul>

              <div class="tab-content mt-2">
                <div class="tab-pane fade show active" id="biodata_pasien" role="tabpanel">
                  <h4>Biodata Pribadi</h4>
                  <div class="form-row">
                    <div class="col form-group">
                      <label>Nomor Induk Kependudukan / KTP</label>
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" name="ktp_daftar" id="ktp_daftar" placeholder=""
                          aria-describedby="basic-addon2">
                        <div class="input-group-append">
                          <button class="btn btn-warning" type="button" id="cek_ktp">CEK KTP</button>
                        </div>
                      </div>
                    </div>
                    <div class="col form-group">
                      <label>Nama Lengkap</label>
                      <input type="text" name="nm_pasien_daftar_baru" class="form-control" placeholder=" "
                        id="nm_pasien_daftar_baru">
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col form-group">
                      <label>Tempat Lahir</label>
                      <input type="text" name="tempat_lahir_baru" class="form-control" placeholder=""
                        id="tempat_lahir_baru">
                    </div>
                    <div class=" col form-group">
                      <label>Tanggal Lahir</label>
                      <input type="text" class="form-control" name="birthday_years" id="birthday_years" placeholder="" disabled>
                    </div>
                    <div class="col form-group">
                      <label>Jenis Kelamin</label>
                      <select id="jenis_kelamin" name="jenis_kelamin" class="form-control">
                        <option value="L">Laki Laki</option>
                        <option value="P">Perempuan</option>
                      </select>
                    </div>
                    <div class="col-2 form-group">
                      <label>Golongan darah</label>
                      <select id="gol_darah" name="gol_darah" class="form-control">
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="AB">AB</option>
                        <option value="O">O</option>
                        <option selected value="-">-</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col-6 form-group">
                      <label>Alamat Lengkap</label>
                      <input class="form-control" placeholder="" id="alamat_daftar_baru"
                        name="alamat_daftar_baru"></input>
                    </div>
                    <div class="col form-group">
                      <label>Kelurahan</label>
                      <input type="text" name="kelurahan" class="form-control" placeholder="" id="kelurahan">
                    </div>
                    <div class=" col form-group">
                      <label>Kecamatan</label>
                      <input type="text" class="form-control" name="kecamatan" id="kecamatan" placeholder="">
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col form-group">
                      <label>Agama</label>
                      <select id="agama" name="agama" class="form-control">
                        <option selected value="ISLAM">Islam</option>
                        <option value="KRISTER">Kristen</option>
                        <option value="KATOLIK">Katolik</option>
                        <option value="HINDU">Hindu</option>
                        <option value="BUDHA">Budha</option>
                        <option value="KONG HU CHU">kong hu chu</option>
                      </select>
                    </div>
                    <div class="col form-group">
                      <label>Status</label>
                      <select id="status_pernikahan" name="status_pernikahan" class="form-control">
                        <option selected value="MENIKAH">Menikah</option>
                        <option value="BELUM MENIKAH">Belum Menikah</option>
                        <option value="JANDA">Janda</option>
                        <option value="DUDHA">Duda</option>
                      </select>
                    </div>
                    <div class="col form-group">
                      <label>Pendidikan</label>
                      <select id="pendidikan" name="pendidikan" class="form-control">
                        <option selected value="TS">TS</option>
                        <option value="TK">TK</option>
                        <option value="SD">SD</option>
                        <option value="SMP">SMP</option>
                        <option value="SMA">SMA</option>
                        <option value="SLTA/SEDERAJAT">SLTA/SEDERAJAT</option>
                        <option value="D1">D1</option>
                        <option value="D2">D2</option>
                        <option value="D3">D3</option>
                        <option value="D4">D4</option>
                        <option value="S1">S1</option>
                        <option value="S2">S2</option>
                        <option value="S2">S3</option>
                      </select>
                    </div>
                    <div class="col form-group">
                      <label>Perkerjaan</label>
                      <select id="perkerjaan" name="perkerjaan" class="form-control">
                        <option selected value="wiraswasta">Wiraswasta</option>
                        <option value="PNS">PNS</option>
                        <option value="pegawai swasta">Pegawai Swasta</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="biodata_penunjang" role="tabpanel">
                  <h4>Informasi Biodata Penunjang</h4>
                  <div class="form-row">
                    <div class="col form-group">
                      <label>Nomor Telepon</label>
                      <input type="text" name="no_telepon_baru" class="form-control" placeholder=""
                        id="no_telepon_baru">
                    </div>
                    <div class="col form-group">
                      <label>Alamat Email</label>
                      <input type="text" name="alamat_email" class="form-control" placeholder=" " id="alamat_email">
                    </div>
                    <div class="col form-group">
                      <label>Alkes/Asuransi</label>
                      <select id="alkes" class="form-control">
                        <option value="-">--Pilih--</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col form-group">
                      <label>Suku Bangsa</label>
                      <select id="suku_bangsa" class="form-control">
                        <option value="-">Pilih suku / Bangsa</option>
                      </select>
                    </div>
                    <div class="col form-group">
                      <label>Bahasa Yang Digunakan</label>
                      <select id="bahasa" class="form-control">
                        <option value="-">Pilih Bahasa</option>
                      </select>
                    </div>
                    <div class="col form-group">
                      <label>Nama ibu kandung</label>
                      <input type="text" name="nm_ibu" class="form-control" placeholder="" id="nm_ibu">
                    </div>
                  </div>
                  <div id="pj">
                    <div class="form-row">
                      <div class="col-3 form-group">
                        <label>Hubungan Penanggung Jawab</label>
                        <select id="hb_pj" class="form-control">
                          <option value="-">--pilih--</option>
                          <option value="AYAH">Ayah</option>
                          <option value="IBU">Ibu</option>
                          <option value="SUAMI">Suami</option>
                          <option value="ISTRI">Istri</option>
                          <option value="SAUDARA">Saudara</option>
                          <option value="ANAK">Anak</option>
                        </select>
                      </div>
                      <div class="col form-group">
                        <label>Nama Penanggung Jawab</label>
                        <input type="text" name="nm_pj" class="form-control" placeholder="" id="nm_pj">
                      </div>
                      <div class="col form-group">
                        <label>Perkerjaan Penaggung Jawab</label>
                        <select id="perkerjaan_pj" name="perkerjaan_pj" class="form-control">
                          <option disabled selected value="-0000">--PILIH--</option>
                          <option value="wiraswasta">Wiraswasta</option>
                          <option value="PNS">PNS</option>
                          <option value="pegawai swasta">Pegawai Swasta</option>
                        </select>
                      </div>
                      <div class="col form-group">
                        <label>Nomer Telepon PJ</label>
                        <input type="text" name="tlp_pj" class="form-control" placeholder="" id="tlp_pj">
                      </div>
                    </div>
                    <div class="col form-group">
                      <label>Alamat Penaggung Jawab</label>
                      <input type="text" name="alm_pj" class="form-control" placeholder="" id="alm_pj">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="review_cetak_baru" role="tabpanel">
                  <h4>Cetak Bukti Daftar</h4>
                  <div style="width: 250px; font-family: Tahoma; margin: 0 auto; font-size: 11px !important;">


                    <div class="block-header">
                      <div style="font-size:12px; font-weight: bold; text-align: center;">
                        <?php echo $dataSettings['nama_instansi']; ?><br>
                        <?php echo $dataSettings['alamat_instansi']; ?><br>
                        <?php echo $dataSettings['kontak']; ?><br>
                        <hr>
                        BUKTI PENDAFTARAN<br>
                        ANJUNGAN PASIEN MANDIRI<br>
                      </div>
                    </div>
                    <hr>
                    <div style="width: 100px; display: inline-block;">Tanggal Daftar</div>: <span
                      id="tgl_daftar_baru"></span><br>
                    <input type="text" name="input_tgl_daftar_baru" class="form-control" id="input_tgl_daftar_baru"
                      hidden>
                    <div style="width: 100px; display: inline-block;">No. RM Sementara</div>: <span
                      id="no_rkm_medis_baru"></span><br>
                    <input type="text" name="input_no_rkm_medis_baru" class="form-control" id="input_no_rkm_medis_baru"
                      hidden>
                    <div style="width: 100px; display: inline-block;">Nama</div>: <span id="nm_pasien_baru"></span><br>
                    <input type="text" name="input_nm_pasien_baru" class="form-control" id="input_nm_pasien_baru"
                      hidden>
                    <div style="text-align: center;">

                      <hr>
                      Terima Kasih Atas kepercayaan Anda.<br>
                      Tunjukan bukti ini ke petugas pendaftaran di rsud tanah abang<br><br>
                    </div>
                    <button style="margin-bottom: 20px;" class="btn btn-primary btn-block" id="cetakPOSBaru">Cetak</button>
                    <!-- <button class="btn btn-primary btn-block" onclick="printDiv('printDaftar');">Cetak</button> -->

                  </div>
                </div>
              </div>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 35%" aria-valuenow="25" aria-valuemin="0"
                  aria-valuemax="100">Langka 1 dari 3</div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button id="lanjut" type="button" class="btn btn-success">LANJUT</button>
            <button id="simpan" type="button" class="btn btn-success" disabled>SIMPAN</button>
            <!-- <button id="cetakPOSBaru" type="button" class="btn btn-success" disabled>CETAK</button> -->
            <button type="button" class="btn btn-danger" data-dismiss="modal"
              onclick="javascript:window.location='<?php echo URL; ?>'">Tutup</button>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="loadMeLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content" style="top: 150px;">
      <div class="modal-body text-center">
        <div class="loader"></div>
        <div clas="loader-txt" id="loader-txt">
          <p>Mohon Menunggu</p>
        </div>
      </div>
    </div>
  </div>
</div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/gijgo.min.js" type="text/javascript"></script>
    <script src="js/jquery.loadingModal.js"></script>
    <script>
      $(document).ready(function () {
        $.ajax({
          type: 'POST',
          url: 'get-alkes.php',
          dataType: "json",
          success: function (data) {
            if (data.status == 'ok') {
              $.each(data.result, function (i, data) {
                var div_data = "<option value=" + data.kd_pj + ">" + data.png_jawab +
                  "</option>";
                $(div_data).appendTo('#alkes');
              });
            } else {
              $("#alkes").empty();
            }
          }
        });

        $.ajax({
          type: 'POST',
          url: 'get-suku.php',
          dataType: "json",
          success: function (data) {
            if (data.status == 'ok') {
              $.each(data.result, function (i, data) {
                var div_data = "<option value=" + data.id + ">" + data.nama_suku_bangsa +
                  "</option>";
                $(div_data).appendTo('#suku_bangsa');
              });
            } else {
              $("#suku_bangsa").empty();
            }
          }
        });

        $.ajax({
          type: 'POST',
          url: 'get-bahasa.php',
          dataType: "json",
          success: function (data) {
            if (data.status == 'ok') {
              $.each(data.result, function (i, data) {
                var div_data = "<option value=" + data.id + ">" + data.nama_bahasa +
                  "</option>";
                $(div_data).appendTo('#bahasa');
              });
            } else {
              $("#bahasa").empty();
            }
          }
        });
        $.ajax({
          type: 'POST',
          url: 'get-cara-bayar.php',
          dataType: "json",
          success: function (data) {
            if (data.status == 'ok') {
              $.each(data.result, function (i, data) {
                var div_data = "<option value=" + data.kd_pj + ">" + data.png_jawab +
                  "</option>";
                $(div_data).appendTo('#pilih_carabayar_daftar');
              });
            } else {
              $("#pilih_carabayar_daftar").empty();
            }
          }
        });

        $.ajax({
          type: 'POST',
          url: 'get-cara-bayar.php',
          dataType: "json",
          success: function (data) {
            if (data.status == 'ok') {
              $.each(data.result, function (i, data) {
                var div_data = "<option value=" + data.kd_pj + ">" + data.png_jawab +
                  "</option>";
                $(div_data).appendTo('#pilih_carabayar_daftar_poli');
              });
            } else {
              $("#pilih_carabayar_daftar_poli").empty();
            }
          }
        });

        $('#daftar_poli').on('shown.bs.modal', '.modal', function () {
          $(this).find('[autofocus]').focus();
        });
      })
    </script>
    <script>
      function printDiv(eleId) {
        var PW = window.open('', '_blank', 'Print content');
        //Use css for print style
        //PW.document.write('<style>.cetak {width: 250px; font-family: Tahoma; margin-top: 10px; margin-right: 5px; margin-bottom: 50px; margin-left: 5px; font-size: 11px !important;}</style>');
        PW.document.write(document.getElementById(eleId).innerHTML);
        PW.document.close();
        PW.focus();
        PW.print();
        PW.close();
        // Redirect after close
        window.location.href = "index.php";
      }
    </script>


    <script>
      function printSEP() {
        var no_sep = document.getElementById("cetak_sep_no_sep").textContent; 
        var PW = window.open('print-SEP.php?no_sep=${no_sep}', '_blank');
        //Use css for print style
        PW.document.write(document.getElementById(eleId).innerHTML);
        PW.document.close();
        PW.focus();
        // PW.print();
        PW.close();
        // Redirect after close
        window.location.href = "index.php";
      }
    </script>

    <script>
      $(function () {
        $('#infoContinue').click(function (e) {
          var no_rkm_medis_daftar = $('#no_rm').val();
          $.ajax({
            type: 'POST',
            url: 'get-daftar.php',
            dataType: "json",
            data: {
              no_rkm_medis_daftar: no_rkm_medis_daftar
            },
            success: function (data) {
              if (data.status == 'ok') {
                e.preventDefault();
                $('#no_rkm_medis_daftar').val(data.result.no_rkm_medis);
                $('#nm_pasien_daftar').val(data.result.nm_pasien);
                $('#alamat_daftar').val(data.result.alamat);
                $('.progress-bar').css('width', '70%');
                $('.progress-bar').html('Langkah 2 dari 3');
                $('#finish').removeAttr("disabled");
                $('#myTab a[href="#poli"]').tab('show');
              } else {
                alert("Detail nomor rekam medik tidak ditemukan...");
                document.getElementById("no_rm").value = "";
              }
            }
          });

          setTimeout(function () {
            window.location.href = "<?php echo URL; ?>";
          }, 500000);

        });

        $('input[name="birthday_years"]').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'yyyy-mm-dd',
          modal: true,
        });

        $('input[name="datepicker"]').datepicker({
          uiLibrary: 'bootstrap4',
          format: 'yyyy-mm-dd',
          modal: true,
          minDate: new Date(),
          change: function (e) {
            var no_rkm_medis_daftar = $('#no_rkm_medis_daftar').val();
            var tgl_registrasi_daftar = $('#tgl_registrasi_daftar').val();
            $.ajax({
              type: 'POST',
              url: 'get-poli.php',
              dataType: "json",
              data: {
                no_rkm_medis: no_rkm_medis_daftar,
                tgl_registrasi: tgl_registrasi_daftar
              },
              success: function (data) {
                if (data.status == 'ok') {
                  $("#pilih_poli_daftar").empty();
                  var div_data = '<option value="">Pilih Poli</option>';
                  $.each(data.result, function (i, data) {
                    div_data += "<option value=" + data.kd_poli + ">" + data.nm_poli + "</option>";
                  });
                  $(div_data).appendTo('#pilih_poli_daftar');
                } else if (data.status == 'exist') {
                  alert("Anda sudah terdaftar...");
                  document.getElementById("tgl_registrasi_daftar").value = "";
                } else {
                  alert("Detail poli tidak ditemukan...");
                  document.getElementById("tgl_registrasi_daftar").value = "";
                }
              }
            });
          }
        });

        $('select#pilih_poli_daftar').on('change', function () {
          var kd_poli_daftar = this.value;
          var tgl_registrasi_daftar = $('#tgl_registrasi_daftar').val();
          $.ajax({
            type: 'POST',
            url: 'get-dokter.php',
            dataType: "json",
            data: {
              kd_poli: kd_poli_daftar,
              tgl_registrasi: tgl_registrasi_daftar
            },
            success: function (data) {
              if (data.status == 'ok') {
                $("#pilih_dokter_daftar").empty();
                $.each(data.result, function (i, data) {
                  var div_data = "<option value=" + data.kd_dokter + ">" + data.nm_dokter +
                    "</option>";
                  $(div_data).appendTo('#pilih_dokter_daftar');
                });
              } else {
                $("#pilih_dokter_daftar").empty();
                alert("Dokter tidak ditemukan...");
              }
            }
          });
        });
        //simpan
        $('#finish').click(function (e) {     
          
          var no_rkm_medis_daftar = $('#no_rkm_medis_daftar').val();
          var nm_pasien_daftar = $('#nm_pasien_daftar').val();
          var alamat_daftar = $('#alamat_daftar').val();
          var tgl_registrasi_daftar = $('#tgl_registrasi_daftar').val();
          var kd_poli_daftar = $('#pilih_poli_daftar').val();
          var kd_dokter_daftar = $('#pilih_dokter_daftar').val();
          var carabayar = $('#pilih_carabayar_daftar').val();

          $('#no_rkm_medis_cetak').text(no_rkm_medis_daftar);
          $('#nm_pasien_cetak').text(nm_pasien_daftar);
          $('#alamat_cetak').text(alamat_daftar);
          $('#tgl_registrasi_cetak').text(tgl_registrasi_daftar);
          $('#kd_poli_cetak').text(kd_poli_daftar);
          $('#kd_dokter_cetak').text(kd_dokter_daftar);

          $.ajax({
            url: 'post-registrasi.php',
            type: 'POST',
            dataType: "json",
            data: {
              no_rkm_medis: no_rkm_medis_daftar,
              tgl_registrasi: tgl_registrasi_daftar,
              kd_poli: kd_poli_daftar,
              kd_dokter: kd_dokter_daftar,
              cara_bayar: carabayar
            },
            success: function (data) {
              if (data.status == 'ok') {
                //alert(data);
                $('#kode_booking_cetak').text(data.result.kd_booking);
                $('#input_kode_booking').val(data.result.kd_booking);
                $('#nm_poli_cetak').text(data.result.nm_poli);
                $('#nm_dokter_cetak').text(data.result.nm_dokter);
                $('#finish').hide();
                $('.progress-bar').css('width', '100%');
                $('.progress-bar').html('Langkah 3 dari 3');
                $('#myTab a[href="#reviewPrint"]').tab('show');
              } else if (data.status == 'exist') {
                alert("Anda sudah terdaftar pada tanggal tersebut");
              }else if(data.status =='null'){
                alert(data.result);
              } else {
                alert("gagal silahkan hubungi petugas")
              }
            }
          });

          setTimeout(function () {
            window.location.href = "<?php echo URL; ?>";
          }, 500000);
        });

        $('#cetakPOS').click(function (e) {
          var kode_booking_final = $('#input_kode_booking').val();
          $.ajax({
            url: 'get_detail_booking.php',
            type: 'POST',
            dataType: "json",
            data: {
              kode_booking: kode_booking_final
            },
            success: function (data) {
              if (data.status == 'ok') {
                //alert(data);
                window.location.href = "<?php echo URL; ?>";
                $('#daftar_penjadwalan').modal('hide');
              } else {
                alert('Exception:', exception);
              }
            }
          });
        });

        $('#cek_ktp').click(function (e) {
          var nik = $('#ktp_daftar').val();
          $.ajax({
            url: 'get-dukcapil.php',
            type: 'POST',
            dataType: "json",
            data: {
              nik: nik
            },
            success: function (data) {
              if (data.status == 'ok') {
                //alert(data);
                $('#nm_pasien_daftar_baru').val(data.result.NAMA_LGKP);
                $('#tempat_lahir_baru').val(data.result.TMPT_LHR);
                $('#alamat_daftar_baru').val(data.result.ALAMAT + " RT." + data.result.NO_RT + " RW." +
                  data.result.NO_RW);
                $('#birthday_years').val(data.tgl_lahir);
                if (data.result.JENIS_KLMIN == 1) {
                  $('#jenis_kelamin').val("L");
                } else {
                  $('#jenis_kelamin').val("P");
                }

                //golongan darah
                if (data.result.DSC_GOL_DRH = 'A') {
                  $('#gol_darah').val("A");
                } else if (data.result.DSC_GOL_DRH = 'B') {
                  $('#gol_darah').val("B");
                } else if (data.result.DSC_GOL_DRH = 'AB') {
                  $('#gol_darah').val("AB");
                } else if (data.result.DSC_GOL_DRH = 'O') {
                  $('#gol_darah').val("O");
                } else {
                  $('#gol_darah').val("-");
                }

                $('#kelurahan').val(data.result.NM_KEL);
                $('#kecamatan').val(data.result.NM_KEC);

                //perkerjaan
                var o = new Option(data.result.DSC_JENIS_PKRJN, data.result.DSC_JENIS_PKRJN);
                $(o).html(data.result.DSC_JENIS_PKRJN);
                $("#perkerjaan").append(o);
                $('#perkerjaan').val(data.result.DSC_JENIS_PKRJN);

                //status
                if (data.result.DSC_STAT_KWN = 'Bekum Kawin') {
                  $('#status_pernikahan').val("BELUM MENIKAH");
                } else if (data.result.DSC_STAT_KWN = 'Kawin') {
                  $('#status_pernikahan').val("MENIKAH");
                }
              } else {
                alert("ktp anda salah silahkan coba lagi / silahkan hubungi petugas")
              }
            }
          });
        });

        //buton lanjut di menu daftar baru
        $('#lanjut').click(function (e) {

          var ktp_daftar = $('#ktp_daftar').val();
          var birthday_years = $('#birthday_years').val();

          $.ajax({
            url: 'cek-pasien.php',
            type: 'POST',
            dataType: "json",
            data: {
              no_ktp: ktp_daftar,
              tgl_lahir: birthday_years
            },
            success: function (data) {
              if (data.status == 'ok') {
                $('.progress-bar').css('width', '70%');
                $('.progress-bar').html('Langkah 2 dari 3');
                $('#lanjut').hide();
                $('#simpan').removeAttr('disabled');
                $('#myTab2 a[href="#biodata_penunjang"]').tab('show');
                var tgl = $('#birthday_years').val();
                tgl = new Date(tgl);
                var today = new Date();
                var age = Math.floor((today - tgl) / (365.25 * 24 * 60 * 60 * 1000));
                if (age > 18) {
                  $('#pj').hide();
                  $('#hb_pj').val('-');
                  $('#nm_pj').val('-');
                  $('#perkerjaan_pj').val('-');
                  $('#tlp_pj').val('-');
                  $('#alm_pj').val('-');
                }
              } else {
                alert(data.result);
              }
            }
          });


        });

        $('#simpan').click(function (e) {
          var ktp_daftar = $('#ktp_daftar').val();
          var nm_pasien_daftar_baru = $('#nm_pasien_daftar_baru').val();
          var tempat_lahir_baru = $('#tempat_lahir_baru').val();
          var birthday_years = $('#birthday_years').val();
          var jenis_kelamin = $('#jenis_kelamin').val();
          var gol_darah = $('#gol_darah').val();
          var alamat_daftar_baru = $('#alamat_daftar_baru').val();
          var kelurahan = $('#kelurahan').val();
          var kecamatan = $('#kecamatan').val();
          var agama = $('#agama').val();
          var status_pernikahan = $('#status_pernikahan').val();
          var pendidikan = $('#pendidikan').val();
          var perkerjaan = $('#perkerjaan').val();
          var no_telepon_baru = $('#no_telepon_baru').val();
          var alamat_email = $('#alamat_email').val();
          var alkes = $('#alkes').val();
          var suku_bangsa = $('#suku_bangsa').val();
          var bahasa = $('#bahasa').val();
          var nm_ibu = $('#nm_ibu').val();
          var hb_pj = $('#hb_pj').val();
          var nm_pj = $('#nm_pj').val();
          var perkerjaan_pj = $('#perkerjaan_pj').val();
          var tlp_pj = $('#tlp_pj').val();
          var alm_pj = $('#alm_pj').val();

          $.ajax({
            url: 'post-registrasi-baru.php',
            type: 'POST',
            dataType: "json",
            data: {
              no_ktp: ktp_daftar,
              nm_pasien: nm_pasien_daftar_baru,
              tlahir: tempat_lahir_baru,
              tgl_lahir: birthday_years,
              jk: jenis_kelamin,
              gol_darah: gol_darah,
              alamat: alamat_daftar_baru,
              kelurahan: kelurahan,
              kecamatan: kecamatan,
              agama: agama,
              status: status_pernikahan,
              penndidikan: pendidikan,
              perkerjaan: perkerjaan,
              no_tlp: no_telepon_baru,
              email: alamat_email,
              kd_pj: alkes,
              suku_bangsa: suku_bangsa,
              bahasa_pasien: bahasa,
              nm_ibu: nm_ibu,
              keluarga: hb_pj,
              nm_pj: nm_pj,
              pj_perkerjaan: perkerjaan_pj,
              tlp_pj: tlp_pj,
              alamat_pj: alm_pj
            },
            success: function (data) {
              if (data.status == 'ok') {
                $('#tgl_daftar_baru').text(data.tanggal);
                $('#no_rkm_medis_baru').text(data.rm);
                $('#nm_pasien_baru').text(data.nama);
                $('#input_tgl_daftar_baru').val(data.tanggal);
                $('#input_no_rkm_medis_baru').val(data.rm);
                $('#input_nm_pasien_baru').val(data.nama);
                $('#simpan').attr("disabled", true);
                $('#cetakPOSBaru').removeAttr('disabled');
                $('.progress-bar').css('width', '100%');
                $('.progress-bar').html('langkah 3 dari 3');
                $('#myTab2 a[href="#review_cetak_baru"]').tab('show');
              } else if (data.status = 'exist') {
                alert("gagal. anda sudah terdaftar, silahkan hubungi petuas")
              } else {
                alert('Exception:', exception);
              }
            }
          });
        });

        $('#cetakPOSBaru').click(function (e) {
          var no_rm_final = $('#input_no_rkm_medis_baru').val();
          var tanggal_daftar = $('#input_tgl_daftar_baru').val();
          var nama_pasien = $('#input_nm_pasien_baru').val();
          $.ajax({
            url: 'get_detail_register.php',
            type: 'POST',
            dataType: "json",
            data: {
              no_rm_final: no_rm_final,
              tanggal_daftar: tanggal_daftar,
              nama_pasien: nama_pasien
            },
            success: function (data) {
              if (data.status == 'ok') {
                //alert(data);
                $('#daftar_baru').modal('hide');
                window.location.href = "<?php echo URL; ?>";
              } else {
                alert('Exception:', exception);
              }
            }
          });
        });
      });
    </script>

    <script>
      $(function () {

        $('#cek_rm').click(function (e) {
          var no_rkm_medis_daftar = $('#no_rm_poli').val();
          $.ajax({
            type: 'POST',
            url: 'get-data-pendaftaran.php',
            dataType: "json",
            data: {
              no_rkm_medis_daftar: no_rkm_medis_daftar
            },
            success: function (data) {
              if (data.status == 'ok') {
                e.preventDefault();
                if (data.method==1) {
                  $('.progress-bar').css('width', '50%');
                  $('.progress-bar').html('Langkah 2 dari 3');
                  $('#myTab a[href="#poli_daftar"]').tab('show');
                  $('#pilih_dokter_poli').attr('disabled', true);
                  $('#pilih_poli_tersedia').attr('disabled', true);
                  $('#no_rkm_medis_poli').val(data.result.no_rkm_medis);
                  $('#nm_pasien_poli').val(data.result.nm_pasien);
                  var div_data1 = "<option selected value=" + data.result.kd_poli + " disable >" + data.result.nm_poli + "</option>";
                  $(div_data1).appendTo('#pilih_dokter_poli');
                  var div_data2 = "<option selected value=" + data.result.kd_dokter + " disable >" + data.result.nm_dokter + "</option>";
                  $(div_data2).appendTo('#pilih_poli_tersedia');
                  $('#kode_booking').text(data.result.kd_booking);
                }else if(data.method==2){
                  $('.progress-bar').css('width', '50%');
                  $('.progress-bar').html('Langkah 2 dari 3');
                  $('#myTab a[href="#poli_daftar"]').tab('show');
                  $('#no_rkm_medis_poli').val(data.pasien.no_rkm_medis);
                  $('#nm_pasien_poli').val(data.pasien.nm_pasien);
                  $.each(data.poli, function (i, data) {
                  var div_data = "<option value=" + data.kd_poli + ">" + data.nm_poli +
                    "</option>";
                  $(div_data).appendTo('#pilih_poli_tersedia');
                  $('#kode_booking').val('-');
                });
                }
              } else {
                alert(data.result);
                document.getElementById("no_rm").value = "";
              }
            }
          });

          setTimeout(function () {
            window.location.href = "<?php echo URL; ?>";
          }, 500000);

        });

        $('select#pilih_poli_tersedia').on('change', function () {
          var kd_poli_daftar = this.value;
          var d = new Date();
          var month = d.getMonth()+1;
          var day = d.getDate();
          var fullyear = d.getFullYear() + '-' +
                    ((''+month).length<2 ? '0' : '') + month + '-' +
                    ((''+day).length<2 ? '0' : '') + day;
          $.ajax({
            type: 'POST',
            url: 'get-dokter.php',
            dataType: "json",
            data: {
              kd_poli: kd_poli_daftar,
              tgl_registrasi: fullyear
            },
            success: function (data) {
              if (data.status == 'ok') {
                $("#pilih_dokter_daftar").empty();
                $.each(data.result, function (i, data) {
                  var div_data = "<option value=" + data.kd_dokter + ">" + data.nm_dokter +
                    "</option>";
                  $(div_data).appendTo('#pilih_dokter_poli');
                });
              } else {
                $("#pilih_dokter_poli").empty();
                alert("Dokter tidak ditemukan...");
              }
            }
          });
        });

        
        $('#simpan_poli').click(function (e) {
          var no_rkm_medis = $('#no_rkm_medis_poli').val();
          var kd_poli = $('#pilih_poli_tersedia').val();
          var kd_dokter = $('#pilih_dokter_poli').val();
          var kd_boking = $('#kode_booking').val();
          var cara_bayar = $('#pilih_carabayar_daftar_poli').val();
          
          $.ajax({
            url: 'post-pendaftaran-umum.php',
            type: 'POST',
            dataType: "json",
            data: {
              no_rkm_medis: no_rkm_medis,
              kd_poli: kd_poli,
              kd_dokter: kd_dokter,
              kd_booking: kd_boking,
              cara_bayar: cara_bayar
            },
            success: function (data) {
              if (data.status == 'ok') {
                //alert(data);
                $('.progress-bar').css('width', '100%');
                  $('.progress-bar').html('Langkah 3 dari 3');
                  $('#myTab a[href="#reviewPrintDaftarPoli"]').tab('show');
                  $('#nm_pasien_rawat_cetak').text(data.result.nm_pasien);
                  $('#no_rkm_medis_rawat_cetak').text(data.result.no_rkm_medis);
                  $('#nm_poli_rawat_cetak').text(data.result.nm_poli);
                  $('#nm_dokter_rawat_cetak').text(data.result.nm_dokter);
                  $('#no_antrian_poli').text(data.result.no_reg);
                  $('#no_rawat_review').text(data.result.no_rawat);
                  $('#input_no_rawat').val(data.result.no_rawat);
                  $('#tgl_berobat').text(data.result.tgl_registrasi);
              } else {
                alert(data.result);
              }
            }
          });
        });

        
        $('#cetakPOSPoli').click(function (e) {
          var no_rawat = $('#input_no_rawat').val();
          $.ajax({
            url: 'get_bukti_register_umum.php',
            type: 'POST',
            dataType: "json",
            data: {
              no_rawat: no_rawat
            },
            success: function (data) {
              if (data.status == 'ok') {
                window.location.href = "<?php echo URL; ?>";
                $('#daftar_poli').modal('hide');
              } else {
                alert(data.result);
              }
            }
          });
        });
      

        $("#no_rawat").keyup(function(event) { 
            if (event.keyCode === 13) { 
                $("#cek_rujukan").click(); 
            } 
        }); 

        $('#cek_rujukan').click(function (e) {
          var no_peserta = $('#no_rawat').val();
          $.ajax({
            type: 'POST',
            url: 'cek-rujukan.php',
            dataType: "json",
            data: {
              no_peserta: no_peserta
            },
            success: function (data) {
              if (data.status == 'ok') {
                //alert(data);
                e.preventDefault();
                var tgl_lahir = new Date(data.result.tanggal_lahir);
                var dd = String(tgl_lahir.getDate()).padStart(2, '0');
                var mm = String(tgl_lahir.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = tgl_lahir.getFullYear();
                tanggal_lahir = dd + '/' + mm + '/' + yyyy;

                var d = new Date();
                var month = d.getMonth()+1;
                var day = d.getDate();
                var fullyear = d.getFullYear() + '-' +
                    ((''+month).length<2 ? '0' : '') + month + '-' +
                    ((''+day).length<2 ? '0' : '') + day;

                $('.progress-bar').css('width', '70%');
                $('.progress-bar').html('Langkah 2 dari 3');
                $('#myTab a[href="#reviewsep"]').tab('show');
                $('#sep_no_rujukan').text(data.result.response.rujukan.noKunjungan);
                $('#sep_tgl_rujukan').text(data.result.response.rujukan.tglKunjungan);
                $('#sep_no_kartu').text(data.result.response.rujukan.peserta.noKartu);
                $('#sep_jkel').text(data.result.response.rujukan.peserta.sex);
                $('#sep_nama_pasien').text(data.result.response.rujukan.peserta.nama);
                $('#sep_jenis_peserta').text(data.result.response.rujukan.peserta.jenisPeserta.keterangan)
                $('#sep_no_rawat').text('-');
                $('#sep_tanggal_lahir').text(data.result.response.rujukan.peserta.tglLahir);
                $('#sep_notelep').text(data.result.response.rujukan.peserta.mr.noTelepon);
                var no_mr_respon = data.result.response.rujukan.peserta.mr.noMR;
                if (no_mr_respon.includes("-")) {
                  no_mr_respon = no_mr_respon.replace(/-/g, '');
                }
                $('#sep_peserta').text(no_mr_respon);
                // $('#sep_nmpolitujuan').val(data.result.response.rujukan.poliRujukan.nama);
                $('#dokter_dpjp_awal').val(data.dokter_awal);
                // if (data.kunjungan_ke >= 1 ) {
                //   $('#sep_nmpolitujuan').attr('disabled', true);
                //   $('#sep_doktertujuan').attr('disabled', true);
                //   var div_data1 = "<option selected value=" + data.result.response.rujukan.poliRujukan.kode + ">" + data.result.response.rujukan.poliRujukan.nama + "</option>";
                //   $(div_data1).appendTo('#sep_nmpolitujuan');
                //   var div_data2 = "<option selected value=" + data.dokter.kd_dokter_bpjs + ">" + data.dokter.nm_dokter + "</option>";
                //   $(div_data2).appendTo('#sep_doktertujuan');
                // }
                $.each(data.poli, function (i, data) {
                  var div_data = "<option value=" + data.kd_poli_bpjs + ">" + data.nm_poli_bpjs +
                    "</option>";
                  $(div_data).appendTo('#sep_nmpolitujuan');
                });
                $('#sep_cob').text('-');
                $('#sep_nmppkrujukan').text(data.result.response.rujukan.provPerujuk.nama);
                $('#sep_kdppkrujukan').text(data.result.response.rujukan.provPerujuk.kode);
                $('#sep_nmdiagnosaawal').text(data.result.response.rujukan.diagnosa.nama);
                $('#sep_kodeicd').text(data.result.response.rujukan.diagnosa.kode);
                $('#sep_kode_pelayanan').text(data.result.response.rujukan.pelayanan.kode);
                $('#sep_nmpelayanan').text(data.result.response.rujukan.pelayanan.nama);
                $('#sep_klsrawat').text('-');
                $('#sep_penjamin').text('-');
                $('#sep_catatan').text('-');
              } else {
                alert(data.result);
                
              }
            }
          });
          setTimeout(function () {
            window.location.href = "<?php echo URL; ?>";
          }, 500000);
        });

        $('select#sep_nmpolitujuan').on('change', function () {
          var kd_poli_bpjs = this.value;
          $.ajax({
            type: 'POST',
            url: 'get-dokter-dpjp.php',
            dataType: "json",
            data: {
              kd_poli_bpjs: kd_poli_bpjs
            },
            success: function (data) {
              if (data.status == 'ok') {
                $("#sep_doktertujuan").empty();
                $.each(data.result, function (i, data) {
                  var div_data = "<option value=" + data.kd_dokter_bpjs + ">" + data.nm_dokter +
                    "</option>";
                  $(div_data).appendTo('#sep_doktertujuan');
                });
              } else {
                $("#sep_doktertujuan").empty();
                alert("Dokter tidak ditemukan...");
              }
            }
          });
        });

      

        $('#simpan_sep').click(function (e) {
        // $("#loadMe").modal({
        //   backdrop: "static", //remove ability to close modal with click
        //   keyboard: false, //remove option to close with keyboard
        //   show: true //Display loader!
        // });
          var noKartu = $('#sep_no_kartu').text();
          var jnsPelayanan = $('#sep_kode_pelayanan').text();
          var noMR = $('#sep_peserta').text();
          var tglRujukan = $('#sep_tgl_rujukan').text();
          var noRujukan = $('#sep_no_rujukan').text();
          var ppkRujukan = $('#sep_kdppkrujukan').text();
          var nmppkrujukan = $('#sep_nmppkrujukan').text();
          var diagAwal = $('#sep_kodeicd').text();
          var tujuan = $('#sep_nmpolitujuan').val();
          var noTelp = $('#sep_notelep').text();
          var kodeDPJP = $('#sep_doktertujuan').val();
          var dokterAwal = $('#dokter_dpjp_awal').val();
          $.ajax({
            url: 'post-rujukan.php',
            type: 'POST',
            dataType: "json",
            data: {
              noKartu: noKartu,
              jnsPelayanan: jnsPelayanan,
              noMR: noMR,
              tglRujukan: tglRujukan,
              noRujukan: noRujukan,
              ppkRujukan: ppkRujukan,
              diagAwal: diagAwal,
              tujuan: tujuan,
              noTelp: noTelp,
              kodeDPJP: kodeDPJP,
              nmppkrujukan: nmppkrujukan,
              dokterAwal: dokterAwal
            },
            success: function (data) {
              // $("#loadMe").modal("hide"); 
              if (data.status == 'ok') {
                //alert(data);
                if(data.result.response.sep.peserta.kelamin == 'Laki-Laki'){
                  var kelamin = 'L';
                }else{
                  var kelamin = 'p';
                }
                $('.progress-bar').css('width', '100%');
                $('.progress-bar').html('Langkah 3 dari 3');
                $('#myTab a[href="#reviewCetak"]').tab('show');
                $('#review_sep_no_sep').text(data.result.response.sep.noSep);
                $('#review_sep_tglsep').text(data.result.response.sep.tglSep);
                $('#review_sep_peserta').text(data.result.response.sep.peserta.jnsPeserta);
                $('#review_sep_no_kartu').text(data.result.response.sep.peserta.noKartu);
                $('#review_sep_tglsep').text(data.result.response.sep.peserta.noMr);
                $('#review_sep_cob').text('-');
                $('#review_sep_nama_pasien').text(data.result.response.sep.peserta.nama);
                $('#review_sep_jenis_rawat').text(data.result.response.sep.jnsPelayanan);
                $('#review_sep_tanggal_lahir').text(data.result.response.sep.peserta.tglLahir);
                $('#review_sep_jk').text(kelamin);
                $('#review_sep_klsrawat').text(data.result.response.sep.kelasRawat);
                $('#review_sep_notelep').text(noTelp);
                $('#review_sep_nmpolitujuan').text(data.result.response.sep.poli);
                $('#review_sep_nmppkrujukan').text(ppkRujukan);
                $('#review_sep_nmdiagnosaawal').text(data.result.response.sep.diagnosa);
                $('#review_sep_catatan').text(data.result.response.sep.catatan);
                $('#review_sep_norawat').text(data.no_rawat);

                $('#cetak_sep_no_sep').text(data.result.response.sep.noSep);
                $('#cetak_sep_tglsep').text(data.result.response.sep.tglSep);
                $('#cetak_sep_peserta').text(data.result.response.sep.peserta.jnsPeserta);
                $('#cetak_sep_no_kartu').text(data.result.response.sep.peserta.noKartu);
                $('#cetak_sep_no_mr').text(data.result.response.sep.peserta.noMr);
                $('#cetak_sep_cob').text('-');
                $('#cetak_sep_nama_pasien').text(data.result.response.sep.peserta.nama);
                $('#cetak_sep_jenis_rawat').text(data.result.response.sep.jnsPelayanan);
                $('#cetak_sep_tanggal_lahir').text(data.result.response.sep.peserta.tglLahir);
                $('#cetak_sep_jk').text(kelamin);
                $('#cetak_sep_klsrawat').text(data.result.response.sep.kelasRawat);
                $('#cetak_sep_notelep').text(noTelp);
                $('#cetak_sep_nmpolitujuan').text(data.result.response.sep.poli);
                $('#cetak_sep_nmppkrujukan').text(ppkRujukan);
                $('#cetak_sep_nmdiagnosaawal').text(data.result.response.sep.diagnosa);
                $('#cetak_sep_catatan').text(data.result.response.sep.catatan);
                $('#cetak_sep_norawat').text(data.no_rawat);
              } else {
                // var error_data = "<p>" + data.result + "</p>";
                //   $(error_data).appendTo('#loader-txt');
                // $("#loadMe").modal("hide");
                alert(data.result);
              }
            }
          });
          setTimeout(function () {
            window.location.href = "<?php echo URL; ?>";
          }, 500000);
        });


        $('#cetakPosBPJS').click(function (e) {
          var no_rawat = $('#review_sep_norawat').text();
          var no_sep = $('#cetak_sep_no_sep').text();
          $.ajax({
            url: 'get_bukti_register_umum.php',
            type: 'POST',
            dataType: "json",
            data: {
              no_rawat :no_rawat
            },
            success: function (data) {
              if (data.status == 'ok') {
                $.ajax({
                  type: 'POST',
                  url: 'print-SEP2.php',
                  data: {
                    no_sep : no_sep
                  },
                  dataType: 'html',
                  success: function (html) {
                  w = window.open(window.location.href,"_blank");
                  w.document.write(html);
                  w.document.close();
                  w.focus();
                  w.print();
                  w.close();
                  window.location.href = "<?php echo URL; ?>";
                  $('#daftar_poli').modal('hide');
                  },
                });
              } else {
                alert(data.result);
              }
            }
          });
        });


      });
    </script>

</body>

</html>