<?php 
include ('config.php');
// $true = true;
// if ($true) {
    if (!empty($_POST['no_sep'])) {
        $getsep = fetch_array(query("select * from bridging_sep where no_sep = '$_POST[no_sep]'"));
        // $getsep = fetch_array(query("select * from bridging_sep where no_sep = '0114R0530220V001949'"));
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test</title>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <style>
        @page 
    {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html
    {
        background-color: #FFFFFF; 
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }

    body
    {
        
        margin: 10mm 15mm 10mm 15mm; /* margin you want for the content */
    }
        @media print {
            
            .col-sm-1,
            .col-sm-2,
            .col-sm-3,
            .col-sm-4,
            .col-sm-5,
            .col-sm-6,
            .col-sm-7,
            .col-sm-8,
            .col-sm-9,
            .col-sm-10,
            .col-sm-11,
            .col-sm-12 {
                float: left !important;
            }

            .col-sm-12 {
                width: 100%;
            }

            .col-sm-11 {
                width: 91.66666666666666%;
            }

            .col-sm-10 {
                width: 83.33333333333334%;
            }

            .col-sm-9 {
                width: 75%;
            }

            .col-sm-8 {
                width: 66.66666666666666%;
            }

            .col-sm-7 {
                width: 58.333333333333336%;
            }

            .col-sm-6 {
                width: 50%;
            }

            .col-sm-5 {
                width: 41.66666666666667%;
            }

            .col-sm-4 {
                width: 33.33333333333333%;
            }

            .col-sm-3 {
                width: 25%;
            }

            .col-sm-2 {
                width: 16.666666666666664%;
            }

            .col-sm-1 {
                width: 8.333333333333332%;
            }

        }
    </style>
</head>
<body>
    <div class="row clearfix">
        <div class="col-sm-4">
            <div class="form-group">
                <img src="img/bpjslogo.png" height="50" width="300">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <h6>
                    SURAT ELEGIBILITAS PESERTA
                </h6>
                <h6>
                    RSUD TANAH ABANG
                </h6>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-2">
            No. SEP
        </div>
        <div class="col-sm-10">
            : <span id="cetak_sep_no_sep"><?php echo $getsep['no_sep']; ?></span>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-2">
            Tgl. SEP
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_tglsep"><?php echo $getsep['tglsep']; ?></span>
        </div>
        <div class="col-sm-2">
            Peserta
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_peserta"><?php echo $getsep['peserta']; ?></span>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-2">
            No. Kartu
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_no_kartu"><?php echo $getsep['no_kartu']; ?></span> (MR. <span id="cetak_sep_no_mr"><?php echo $getsep['nomr']; ?></span>
        </div>
        <div class="col-sm-2">
            COB
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_cob"><?php echo ($getsep['cob'] == '0') ? '-':$cob;?></span>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-2">
            Nama Peserta
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_nama_pasien"><?php echo $getsep['nama_pasien']; ?></span>
        </div>
        <div class="col-sm-2">
            Jns. Rawat
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_jenis_rawat"><?php echo ($getsep['jnspelayanan'] == '1') ? 'Rawat Inap':'Rawat Jalan'; ?></span>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-2">
            Tgl. Lahir
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_tanggal_lahir"><?php echo $getsep['tanggal_lahir']; ?></span> Kelamin : <span id="cetak_sep_jk"><?php echo $getsep['jkel']; ?></span>
        </div>
        <div class="col-sm-2">
            Kls. Rawat
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_klsrawat"><?php echo $getsep['klsrawat']; ?></span>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-2">
            No.Telepon
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_notelep"><?php echo $getsep['notelep']; ?></span>
        </div>
        <div class="col-sm-2">
            No.Rawat
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_norawat"><?php echo $getsep['no_rawat']; ?></span>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-2">
            Sub/Spesialis
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_nmpolitujuan"><?php echo $getsep['nmpolitujuan']; ?></span>
        </div>
        <div class="col-sm-2">
            Penjamin
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_penjamin"><?php echo $getsep['penjamin']; ?></span>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-2">
            Faskes Perujuk
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_nmppkrujukan"><?php echo $getsep['nmppkrujukan']; ?></span>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-2">
            Diagnosa Awal
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_nsmiagnosaawal"><?php echo $getsep['nmdiagnosaawal']; ?></span>
        </div>
    </div>
    <div class="row clearfix">


    </div>
    <div class="row clearfix">
        <div class="col-sm-2">
            Catatan
        </div>
        <div class="col-sm-4">
            : <span id="cetak_sep_catatan"><?php echo $getsep['catatan']; ?></span>
        </div>
        <div class="col-sm-6" style="left: 90px;">
            Pasien/Keluarga Pasien
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-6">
            <small><em>
                    <font size="1px">*Saya Menyetujui BPJS Kesehatan menggunakan informasi
                        Medis
                        Pasien jika diperlukan.</font>
                </em></small>
        </div>
        <div class="col-sm-6">
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-6">
        </div>
        <div class="col-sm-6">
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-6">
            <small><em>
                    <font size="1px">**SEP bukan sebagai bukti penjaminan peserta</font>
                </em></small>
        </div>
        <div class="col-sm-6">
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-2">
            <small><em>
                    <font size="1px">Cetakan ke 1</font>
                </em></small>
        </div>
        <div class="col-sm-10">
            <small><em>
                    <font size="1px"><?php echo date("d/m/Y h:i:s A");?></font>
                </em></small>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-8">
            <center>----------------------------------------------------</center>
        </div>
    </div>
    <script>
	window.load = window.print();
  </script>	
</body>

</html>

    <?php } ?>