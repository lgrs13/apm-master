    <?php
include ('config.php');
if(!empty($_POST['no_rkm_medis'])){
    $nomer = $_POST['no_rkm_medis'];
    date_default_timezone_set('Asia/Jakarta');
    $today = date("Y-m-d");
    $data = array();
    $kd_booking = $_POST['kd_booking'];
    $cek_bk = substr($_POST['kd_booking'],0,2);
    if ($cek_bk == 'BK') {
        $query = $db->query("SELECT * FROM booking_registrasi WHERE kd_booking = '$kd_booking'");
        if($query->num_rows > 0){
            $cek = fetch_assoc(query("SELECT * FROM booking_registrasi WHERE kd_booking='$kd_booking'"));
            //cek apakah tanggal benar
            if ($cek['tanggal_periksa'] == date("Y-m-d")) {
                //cek apakah status belum hangus/ terdaftar
                if ($cek['status'] == 'Belum') {
                    //update umur pasien
                    $uptade = (query("update pasien set umur=CONCAT(CONCAT(CONCAT(TIMESTAMPDIFF(YEAR, tgl_lahir, CURDATE()), ' Th '),CONCAT(TIMESTAMPDIFF(MONTH, tgl_lahir, CURDATE()) - ((TIMESTAMPDIFF(MONTH, tgl_lahir, CURDATE()) div 12) * 12), ' Bl ')),CONCAT(TIMESTAMPDIFF(DAY, DATE_ADD(DATE_ADD(tgl_lahir,INTERVAL TIMESTAMPDIFF(YEAR, tgl_lahir, CURDATE()) YEAR), INTERVAL TIMESTAMPDIFF(MONTH, tgl_lahir, CURDATE()) - ((TIMESTAMPDIFF(MONTH, tgl_lahir, CURDATE()) div 12) * 12) MONTH), CURDATE()), ' Hr')) WHERE no_rkm_medis = '$nomer'"));
                    // echo $uptade;
                    $status = fetch_assoc(query("select if((select count(no_rkm_medis) from reg_periksa where no_rkm_medis='$nomer' and kd_poli='$cek[kd_poli]')>0,'Lama','Baru') as status"));
                    // echo $status;
                    $cek_norawat = fetch_array(query("select ifnull(MAX(CONVERT(RIGHT(no_rawat,6),signed)),0) as norawat from reg_periksa where tgl_registrasi='$cek[tanggal_periksa]'"));
                    $norawat = str_replace("-","/",$cek['tanggal_periksa'])."/".str_pad($cek_norawat['norawat'] +1, 6, "0", STR_PAD_LEFT);
                    // echo $norawat;
                    $cek_noreg = fetch_array(query("select ifnull(MAX(CONVERT(no_reg,signed)),0) as noreg  from reg_periksa where kd_dokter='$cek[kd_dokter]' and tgl_registrasi='$cek[tanggal_periksa]'"));
                    $nreg = $cek_noreg['noreg'] +1;
                    $noreg = str_pad($nreg, 3, "0", STR_PAD_LEFT);
                    // echo $noreg;
                    $get_pasien = fetch_array(query("SELECT * FROM pasien WHERE no_rkm_medis = '{$cek['no_rkm_medis']}'"));
                    list($cY, $cm, $cd) = explode('-', date('Y-m-d'));
                    list($Y, $m, $d) = explode('-', date('Y-m-d', strtotime($get_pasien['tgl_lahir'])));
                    $umurdaftar = $cY - $Y;
    
                    $biaya_reg=fetch_array(query("SELECT registrasilama FROM poliklinik WHERE kd_poli='{$cek['kd_poli']}'"));
    
                    // simpan data ke reg periksa
                    $insert = query("INSERT
                        INTO
                        reg_periksa
                        SET
                        no_reg          = '$noreg',
                        no_rawat        = '$norawat',
                        tgl_registrasi  = '{$cek['tanggal_periksa']}',
                        jam_reg         = '$time',
                        kd_dokter       = '{$cek['kd_dokter']}',
                        no_rkm_medis    = '{$cek['no_rkm_medis']}',
                        kd_poli         = '{$cek['kd_poli']}',
                        p_jawab         = '{$get_pasien['namakeluarga']}',
                        almt_pj         = '{$get_pasien['alamat']}',
                        hubunganpj      = '{$get_pasien['keluarga']}',
                        biaya_reg       = '{$biaya_reg['0']}',
                        stts            = 'Belum',
                        stts_daftar     = 'Lama',
                        status_lanjut   = 'Ralan',
                        kd_pj           = '$_POST[cara_bayar]',
                        umurdaftar      = '{$umurdaftar}',
                        sttsumur        = 'Th',
                        status_bayar    = 'Belum Bayar',
                        status_poli     = '{$status['status']}',
                        posisi          = '-'
                    ");
    
                    if ($insert) {
                        $query = $db->query(
                            "SELECT
                            f.no_rkm_medis,
                            f.nm_pasien,
                            a.tgl_registrasi,
                            a.jam_reg,
                            a.no_rawat,
                            a.no_reg,
                            b.nm_poli,
                            c.nm_dokter,
                            a.status_lanjut,
                            d.png_jawab
                            FROM reg_periksa a
                            LEFT JOIN poliklinik b ON a.kd_poli = b.kd_poli
                            LEFT JOIN dokter c ON a.kd_dokter = c.kd_dokter
                            LEFT JOIN penjab d ON a.kd_pj = d.kd_pj
                            LEFT JOIN pasien f ON a.no_rkm_medis = f.no_rkm_medis
                            WHERE a.no_rawat = '{$norawat}'
                        ");
    
                        if($query->num_rows > 0){
                            $userData = $query->fetch_assoc();
                            $data['status'] = 'ok';
                            $data['method'] = '1';
                            // 1 = booking 2=nonboking
                            $data['result'] = $userData;
                        }else {
                            $data['status'] = 'err';
                            $data['result'] = 'pendaftaran Gagal, Silahkan hubungi petugas';
                        }
                    }else {
                        $data['status'] = 'err';
                        $data['result'] = 'pendaftaran Gagal, Silahkan hubungi petugas';
                    }
                //eroor status selain belum
                }else{
                    $data['status'] = 'err';
                    $data['result'] = 'pendaftaran Gagal, status booking anda salah Silahkan hubungi petugas';
                }
            //error tanggal salah
            }else {
                $data['status'] = 'err';
                $data['result'] = 'pendaftaran Gagal, tanggal tidak benar Silahkan hubungi petugas';
            }
        //error KD booking   
        }else{
            $data['status'] = 'err';
            $data['result'] = 'pendaftaran Gagal, kode booking tidak ditemukan Silahkan hubungi petugas';
        }
    }else{
        $data_pasien = fetch_assoc(query("SELECT * FROM pasien WHERE no_rkm_medis = '$nomer'"));
        $cek_data = num_rows(query("SELECT no_rawat FROM reg_periksa WHERE tgl_registrasi = '$today' AND kd_dokter = '$_POST[kd_dokter]' AND kd_poli ='$_POST[kd_poli]' AND no_rkm_medis ='$_POST[no_rkm_medis]'"));
        if($cek_data==0){
            $uptade = (query("update pasien set umur=CONCAT(CONCAT(CONCAT(TIMESTAMPDIFF(YEAR, tgl_lahir, CURDATE()), ' Th '),CONCAT(TIMESTAMPDIFF(MONTH, tgl_lahir, CURDATE()) - ((TIMESTAMPDIFF(MONTH, tgl_lahir, CURDATE()) div 12) * 12), ' Bl ')),CONCAT(TIMESTAMPDIFF(DAY, DATE_ADD(DATE_ADD(tgl_lahir,INTERVAL TIMESTAMPDIFF(YEAR, tgl_lahir, CURDATE()) YEAR), INTERVAL TIMESTAMPDIFF(MONTH, tgl_lahir, CURDATE()) - ((TIMESTAMPDIFF(MONTH, tgl_lahir, CURDATE()) div 12) * 12) MONTH), CURDATE()), ' Hr')) WHERE no_rkm_medis = '$nomer'"));
            // echo $uptade;
            $status = fetch_assoc(query("select if((select count(no_rkm_medis) from reg_periksa where no_rkm_medis='$nomer' and kd_poli='$_POST[kd_poli]')>0,'Lama','Baru') as status"));
            // echo $status;
            $cek_norawat = fetch_array(query("select ifnull(MAX(CONVERT(RIGHT(no_rawat,6),signed)),0) as norawat from reg_periksa where tgl_registrasi='$today'"));
            $norawat = str_replace("-","/",$today)."/".str_pad($cek_norawat['norawat'] +1, 6, "0", STR_PAD_LEFT);
            // echo $norawat;
            $cek_noreg = fetch_array(query("select ifnull(MAX(CONVERT(no_reg,signed)),0) as noreg  from reg_periksa where kd_dokter='$_POST[kd_dokter]' and tgl_registrasi='$today'"));
            $nreg = $cek_noreg['noreg'] +1;
            $noreg = str_pad($nreg, 3, "0", STR_PAD_LEFT);
            // echo $noreg;
            $get_pasien = fetch_array(query("SELECT * FROM pasien WHERE no_rkm_medis = '$nomer'"));
            list($cY, $cm, $cd) = explode('-', date('Y-m-d'));
            list($Y, $m, $d) = explode('-', date('Y-m-d', strtotime($get_pasien['tgl_lahir'])));
            $umurdaftar = $cY - $Y;
    
            $biaya_reg=fetch_array(query("SELECT registrasilama FROM poliklinik WHERE kd_poli='$_POST[kd_poli]'"));

            $insert = query("INSERT
                        INTO
                        reg_periksa
                        SET
                        no_reg          = '$noreg',
                        no_rawat        = '$norawat',
                        tgl_registrasi  = '$today',
                        jam_reg         = '$time',
                        kd_dokter       = '$_POST[kd_dokter]',
                        no_rkm_medis    = '$nomer',
                        kd_poli         = '$_POST[kd_poli]',
                        p_jawab         = '{$get_pasien['namakeluarga']}',
                        almt_pj         = '{$get_pasien['alamat']}',
                        hubunganpj      = '{$get_pasien['keluarga']}',
                        biaya_reg       = '{$biaya_reg['0']}',
                        stts            = 'Belum',
                        stts_daftar     = 'Lama',
                        status_lanjut   = 'Ralan',
                        kd_pj           = '$_POST[cara_bayar]',
                        umurdaftar      = '{$umurdaftar}',
                        sttsumur        = 'Th',
                        status_bayar    = 'Belum Bayar',
                        status_poli     = '{$status['status']}',
                        posisi          = '-' 
                        
                    ");

            if ($insert) {
                $query = $db->query(
                    "SELECT
                    f.no_rkm_medis,
                    f.nm_pasien,
                    a.tgl_registrasi,
                    a.jam_reg,
                    a.no_rawat,
                    a.no_reg,
                    b.nm_poli,
                    c.nm_dokter,
                    a.status_lanjut,
                    d.png_jawab
                    FROM reg_periksa a
                    LEFT JOIN poliklinik b ON a.kd_poli = b.kd_poli
                    LEFT JOIN dokter c ON a.kd_dokter = c.kd_dokter
                    LEFT JOIN penjab d ON a.kd_pj = d.kd_pj
                    LEFT JOIN pasien f ON a.no_rkm_medis = f.no_rkm_medis
                    WHERE a.no_rawat = '{$norawat}'
                ");

                if($query->num_rows > 0){
                    $userData = $query->fetch_assoc();
                    $data['status'] = 'ok';
                    $data['method'] = '2';
                    // 1 = booking 2=nonboking
                    $data['result'] = $userData;
                }else {
                    $data['status'] = 'err';
                    $data['result'] = 'pendaftaran Gagal, Silahkan hubungi petugas';
                }
            }else {
                $data['status'] = 'err';
                $data['result'] = 'pendaftaran Gagal, Silahkan hubungi petugas';
            }
        }else{
            $data['status'] = 'err';
            $data['result'] = 'pendaftaran Gagal, anda sudah terdaftar, Silahkan hubungi petugas';
        }

    }
        //returns data as JSON format
        echo json_encode($data);
}
?>