<?php
include ('config.php');
require __DIR__ . '/plugins/escpos/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;


try {
    // Enter the share name for your printer here, as a smb:// url format
    $connector = new WindowsPrintConnector("smb://guest@192.168.13.2/RM LQ 310");
    //$connector = new WindowsPrintConnector("smb://Guest@computername/Receipt Printer");
    //$connector = new WindowsPrintConnector("smb://FooUser:secret@computername/workgroup/Receipt Printer");
    //$connector = new WindowsPrintConnector("smb://User:secret@computername/Receipt Printer");
    
    /* Print a "Hello world" receipt" */
    $printer = new Printer($connector);
    $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        $printer->text("RUMAH SAKIT UMUM DAERAH \n TANAH ABANG \n");
        $printer->selectPrintMode(printer::MODE_FONT_A);
        $printer->text("BUKTI REGISTER PENDAFTARAN\n");
        $printer->feed();
    // $printer -> cut();
    
    /* Close printer */
    $printer -> close();
} catch (Exception $e) {
    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
}
    

?>
