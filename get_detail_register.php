<?php
include ('config.php');
require __DIR__ . '/plugins/escpos/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

    if(!empty($_POST['no_rm_final'])){


    try {
        $bookcode = $_POST['no_rm_final'];
        // Enter the share name for your USB printer here
        // $connector = null;
        $connector=new WindowsPrintConnector("THERMAL");
        // $connector = new WindowsPrintConnector("smb://sisan@192.168.11.8/THERMAL");
        /* Print a "Hello world" receipt" */
        $printer=new Printer($connector);

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        $printer->text("RUMAH SAKIT UMUM DAERAH \n TANAH ABANG \n");
        $printer->selectPrintMode(printer::MODE_FONT_A);
        $printer->text("ANJUNGAN PENDAFTARAN MANDIRI\n");
            
        $printer->feed();
        $printer->feed();   

        // $printer->setEmphasis(true);
        // $printer->text($bookcode);
        // $printer->setEmphasis(false);
        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->text("No. RM      : ".$_POST['no_rm_final']."\n");
        $printer->text("Nama            : ".$_POST['tanggal_daftar']."\n");
        $printer->text("Tanggal Daftar : ".$_POST['nama_pasien']."\n");

        $printer->feed();
        $printer->feed();

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_ABOVE);
        $printer->barcode($bookcode, Printer::BARCODE_CODE39);
        $printer->feed();
        $printer->selectPrintMode(Printer::MODE_EMPHASIZED);

        $printer->text("Teima kasih atas kepercayaan Anda");
        $printer->text("Tunjukan bukti ini ke petugas pendaftaran di RSUD tanah abang");

        $printer->feed();
        $printer->feed();
        $printer->feed();
        $printer->cut();
        $printer->close();
    }

    catch (Exception $e) {
        $bookcode = "";
    }
    
    
    if($bookcode != ""){
        $data['status'] = 'ok';
    }else{
        $data['status'] = 'err';
    }
    //returns data as JSON format
    echo json_encode($data);
}
?>
