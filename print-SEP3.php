<?php 
include ('config.php');
// $true = true;
// if ($true) {
    if (!empty($_POST['no_sep'])) {
        $getsep = fetch_array(query("select * from bridging_sep where no_sep = '$_POST[no_sep]'"));
        // $getsep = fetch_array(query("select * from bridging_sep where no_sep = '0114R0530520V000595'"));
?>

<html lang="en">

<head>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <style>
    </style>

    <style>
        @page {
            size: auto;
            /* auto is the initial value */
            margin: 0mm;
            /* this affects the margin in the printer settings */
        }

        html {
            background-color: #FFFFFF;
            margin: 0px;
            /* this affects the margin on the html before sending to printer */
        }

        body {

            margin: 5mm 10mm 5mm 10mm;
            /* margin you want for the content */
        }

        .judul {
            width: 190px;
        }

        .titikdua {
            width: 0px;
        }

        .isi {
            width: 290px;
        }
    </style>
</head>

<body>
    <table>
        <tbody>
            <tr>
                <td colspan="3"><img src="img/bpjslogo.png" height="50" width="300"></td>
                <td colspan="3">
                    <h5>SURAT ELEGIBILITAS PESERTA</h5>
                    <h5>RSUD TANAH ABANG</h5>
                </td>
            </tr>
            <table>
                <tbody>
                    <tr>
                        <td class="judul">No. SEP</td>
                        <td class="titikdua">:</td>
                        <td class="isi"><span id="cetak_sep_no_sep"><?php echo $getsep['no_sep']; ?></span></td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td>Tgl. SEP</td>
                        <td>:</td>
                        <td><span id="cetak_sep_tglsep"><?php echo $getsep['tglsep']; ?></span></td>

                        <td class="judul">Peserta</td>
                        <td class="titikdua">:</td>
                        <td class="isi"><span id="cetak_sep_peserta"><?php echo $getsep['peserta']; ?></td>
                    </tr>
                    <tr>
                        <td>No. kartu</td>
                        <td>:</td>
                        <td><span id="cetak_sep_no_kartu"><?php echo $getsep['no_kartu']; ?></span><span>(MR. <span
                                    id="cetak_sep_no_mr"><?php echo $getsep['nomr']; ?>)</span></td>

                        <td>COB</td>
                        <td>:</td>
                        <td><span id="cetak_sep_cob"><?php echo ($getsep['cob'] == '0') ? '-':$cob;?></td>
                    </tr>
                    <tr>
                        <td>Nama Peserta</td>
                        <td>:</td>
                        <td><span id="cetak_sep_nama_pasien"><?php echo $getsep['nama_pasien']; ?></span></td>

                        <td>Jns. Rawat</td>
                        <td>:</td>
                        <td><span id="cetak_sep_jenis_rawat"><?php echo ($getsep['jnspelayanan'] == '1') ? 'Rawat Inap':'Rawat Jalan'; ?></td>
                    </tr>
                    <tr>
                        <td>Tgl. Lahir</td>
                        <td>:</td>
                        <td><span id="cetak_sep_tanggal_lahir"><?php echo $getsep['tanggal_lahir']; ?></span></span>
                            Kelamin : <span id="cetak_sep_jk"><?php echo $getsep['jkel']; ?></span></td>

                        <td>Kls. Rawat</td>
                        <td>:</td>
                        <td><span id="cetak_sep_klsrawat"><?php echo $getsep['klsrawat'];?></td>
                    </tr>
                    <tr>
                        <td>No.Telepon</td>
                        <td>:</td>
                        <td><span id="cetak_sep_notelep"><?php echo $getsep['notelep']; ?></span></td>

                        <td>No.Rawat</td>
                        <td>:</td>
                        <td><span id="cetak_sep_norawat"><?php echo $getsep['no_rawat'];?></td>
                    </tr>
                    <tr>
                        <td>Sub/Spesialis</td>
                        <td>:</td>
                        <td><span id="cetak_sep_nmpolitujuan"><?php echo $getsep['nmpolitujuan']; ?></span></td>

                        <td>Penjamin</td>
                        <td>:</td>
                        <td><span id="cetak_sep_norawat"><?php echo $getsep['penjamin'];?></td>
                    </tr>
                    <tr>
                        <td>Faskes Perujuk</td>
                        <td>:</td>
                        <td><span id="cetak_sep_nmppkrujukan"><?php echo $getsep['nmppkrujukan']; ?></span></td>
                    </tr>
                    <tr>
                        <td>Diagnosa Awal</td>
                        <td>:</td>
                        <td><span id="cetak_sep_nsmiagnosaawal"><?php echo $getsep['nmdiagnosaawal']; ?></span></td>
                    </tr>
                    <tr>
                        <td>Catatan</td>
                        <td>:</td>
                        <td><span id="cetak_sep_catatan"><?php echo $getsep['catatan']; ?></span></td>


                        <td colspan="3">
                            <center>Pasien/Keluarga Pasien</center>
                        </td>

                    </tr>
                    <td colspan="3"><small><em>
                                <font size="1px">*Saya Menyetujui BPJS Kesehatan menggunakan informasi
                                    Medis
                                    Pasien jika diperlukan.</font>
                            </em></small></td>
                    <tr>
                        <td colspan="3">
                            <small><em>
                                    <font size="1px">**SEP bukan sebagai bukti penjaminan peserta</font>
                                </em></small>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="0">
                            <small><em>
                                    <font size="1px">Cetakan ke 1</font>
                                </em></small>
                        </td>
                        <td></td>
                        <td>
                            <small><em>
                                    <font size="1px"><?php echo date("d/m/Y h:i:s A");?></font>
                                </em></small>
                        </td>
                        <td colspan="3">
                            <center>----------------------------------------------------</center>
                        </td>
                    </tr>
                </tbody>
            </table>

        </tbody>
    </table>

    
    ---------------------------------------------------------------------------------------------------------------------------------------------


    <table>
        <tbody>
            <tr>
                <td colspan="3"><img src="img/bpjslogo.png" height="50" width="300"></td>
                <td colspan="3">
                    <h5>SURAT ELEGIBILITAS PESERTA</h5>
                    <h5>RSUD TANAH ABANG</h5>
                </td>
            </tr>
            <table>
                <tbody>
                    <tr>
                        <td class="judul">No. SEP</td>
                        <td class="titikdua">:</td>
                        <td class="isi"><span id="cetak_sep_no_sep"><?php echo $getsep['no_sep']; ?></span></td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td>Tgl. SEP</td>
                        <td>:</td>
                        <td><span id="cetak_sep_tglsep"><?php echo $getsep['tglsep']; ?></span></td>

                        <td class="judul">Peserta</td>
                        <td class="titikdua">:</td>
                        <td class="isi"><span id="cetak_sep_peserta"><?php echo $getsep['peserta']; ?></td>
                    </tr>
                    <tr>
                        <td>No. kartu</td>
                        <td>:</td>
                        <td><span id="cetak_sep_no_kartu"><?php echo $getsep['no_kartu']; ?></span><span>(MR. <span
                                    id="cetak_sep_no_mr"><?php echo $getsep['nomr']; ?>)</span></td>

                        <td>COB</td>
                        <td>:</td>
                        <td><span id="cetak_sep_cob"><?php echo ($getsep['cob'] == '0') ? '-':$cob;?></td>
                    </tr>
                    <tr>
                        <td>Nama Peserta</td>
                        <td>:</td>
                        <td><span id="cetak_sep_nama_pasien"><?php echo $getsep['nama_pasien']; ?></span></td>

                        <td>Jns. Rawat</td>
                        <td>:</td>
                        <td><span id="cetak_sep_jenis_rawat"><?php echo ($getsep['jnspelayanan'] == '1') ? 'Rawat Inap':'Rawat Jalan'; ?></td>
                    </tr>
                    <tr>
                        <td>Tgl. Lahir</td>
                        <td>:</td>
                        <td><span id="cetak_sep_tanggal_lahir"><?php echo $getsep['tanggal_lahir']; ?></span></span>
                            Kelamin : <span id="cetak_sep_jk"><?php echo $getsep['jkel']; ?></span></td>

                        <td>Kls. Rawat</td>
                        <td>:</td>
                        <td><span id="cetak_sep_klsrawat"><?php echo $getsep['klsrawat'];?></td>
                    </tr>
                    <tr>
                        <td>No.Telepon</td>
                        <td>:</td>
                        <td><span id="cetak_sep_notelep"><?php echo $getsep['notelep']; ?></span></td>

                        <td>No.Rawat</td>
                        <td>:</td>
                        <td><span id="cetak_sep_norawat"><?php echo $getsep['no_rawat'];?></td>
                    </tr>
                    <tr>
                        <td>Sub/Spesialis</td>
                        <td>:</td>
                        <td><span id="cetak_sep_nmpolitujuan"><?php echo $getsep['nmpolitujuan']; ?></span></td>

                        <td>Penjamin</td>
                        <td>:</td>
                        <td><span id="cetak_sep_norawat"><?php echo $getsep['penjamin'];?></td>
                    </tr>
                    <tr>
                        <td>Faskes Perujuk</td>
                        <td>:</td>
                        <td><span id="cetak_sep_nmppkrujukan"><?php echo $getsep['nmppkrujukan']; ?></span></td>
                    </tr>
                    <tr>
                        <td>Diagnosa Awal</td>
                        <td>:</td>
                        <td><span id="cetak_sep_nsmiagnosaawal"><?php echo $getsep['nmdiagnosaawal']; ?></span></td>
                    </tr>
                    <tr>
                        <td>Catatan</td>
                        <td>:</td>
                        <td><span id="cetak_sep_catatan"><?php echo $getsep['catatan']; ?></span></td>


                        <td colspan="3">
                            <center>Pasien/Keluarga Pasien</center>
                        </td>

                    </tr>
                    <td colspan="3"><small><em>
                                <font size="1px">*Saya Menyetujui BPJS Kesehatan menggunakan informasi
                                    Medis
                                    Pasien jika diperlukan.</font>
                            </em></small></td>
                    <tr>
                        <td colspan="3">
                            <small><em>
                                    <font size="1px">**SEP bukan sebagai bukti penjaminan peserta</font>
                                </em></small>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="0">
                            <small><em>
                                    <font size="1px">Cetakan ke 1</font>
                                </em></small>
                        </td>
                        <td></td>
                        <td>
                            <small><em>
                                    <font size="1px"><?php echo date("d/m/Y h:i:s A");?></font>
                                </em></small>
                        </td>
                        <td colspan="3">
                            <center>----------------------------------------------------</center>
                        </td>
                    </tr>
                </tbody>
            </table>

        </tbody>
    </table>

    
    ---------------------------------------------------------------------------------------------------------------------------------------------


    <table>
        <tbody>
            <tr>
                <td colspan="3"><img src="img/bpjslogo.png" height="50" width="300"></td>
                <td colspan="3">
                    <h5>SURAT ELEGIBILITAS PESERTA</h5>
                    <h5>RSUD TANAH ABANG</h5>
                </td>
            </tr>
            <table>
                <tbody>
                    <tr>
                        <td class="judul">No. SEP</td>
                        <td class="titikdua">:</td>
                        <td class="isi"><span id="cetak_sep_no_sep"><?php echo $getsep['no_sep']; ?></span></td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td>Tgl. SEP</td>
                        <td>:</td>
                        <td><span id="cetak_sep_tglsep"><?php echo $getsep['tglsep']; ?></span></td>

                        <td class="judul">Peserta</td>
                        <td class="titikdua">:</td>
                        <td class="isi"><span id="cetak_sep_peserta"><?php echo $getsep['peserta']; ?></td>
                    </tr>
                    <tr>
                        <td>No. kartu</td>
                        <td>:</td>
                        <td><span id="cetak_sep_no_kartu"><?php echo $getsep['no_kartu']; ?></span><span>(MR. <span
                                    id="cetak_sep_no_mr"><?php echo $getsep['nomr']; ?>)</span></td>

                        <td>COB</td>
                        <td>:</td>
                        <td><span id="cetak_sep_cob"><?php echo ($getsep['cob'] == '0') ? '-':$cob;?></td>
                    </tr>
                    <tr>
                        <td>Nama Peserta</td>
                        <td>:</td>
                        <td><span id="cetak_sep_nama_pasien"><?php echo $getsep['nama_pasien']; ?></span></td>

                        <td>Jns. Rawat</td>
                        <td>:</td>
                        <td><span id="cetak_sep_jenis_rawat"><?php echo ($getsep['jnspelayanan'] == '1') ? 'Rawat Inap':'Rawat Jalan'; ?></td>
                    </tr>
                    <tr>
                        <td>Tgl. Lahir</td>
                        <td>:</td>
                        <td><span id="cetak_sep_tanggal_lahir"><?php echo $getsep['tanggal_lahir']; ?></span></span>
                            Kelamin : <span id="cetak_sep_jk"><?php echo $getsep['jkel']; ?></span></td>

                        <td>Kls. Rawat</td>
                        <td>:</td>
                        <td><span id="cetak_sep_klsrawat"><?php echo $getsep['klsrawat'];?></td>
                    </tr>
                    <tr>
                        <td>No.Telepon</td>
                        <td>:</td>
                        <td><span id="cetak_sep_notelep"><?php echo $getsep['notelep']; ?></span></td>

                        <td>No.Rawat</td>
                        <td>:</td>
                        <td><span id="cetak_sep_norawat"><?php echo $getsep['no_rawat'];?></td>
                    </tr>
                    <tr>
                        <td>Sub/Spesialis</td>
                        <td>:</td>
                        <td><span id="cetak_sep_nmpolitujuan"><?php echo $getsep['nmpolitujuan']; ?></span></td>

                        <td>Penjamin</td>
                        <td>:</td>
                        <td><span id="cetak_sep_norawat"><?php echo $getsep['penjamin'];?></td>
                    </tr>
                    <tr>
                        <td>Faskes Perujuk</td>
                        <td>:</td>
                        <td><span id="cetak_sep_nmppkrujukan"><?php echo $getsep['nmppkrujukan']; ?></span></td>
                    </tr>
                    <tr>
                        <td>Diagnosa Awal</td>
                        <td>:</td>
                        <td><span id="cetak_sep_nsmiagnosaawal"><?php echo $getsep['nmdiagnosaawal']; ?></span></td>
                    </tr>
                    <tr>
                        <td>Catatan</td>
                        <td>:</td>
                        <td><span id="cetak_sep_catatan"><?php echo $getsep['catatan']; ?></span></td>


                        <td colspan="3">
                            <center>Pasien/Keluarga Pasien</center>
                        </td>

                    </tr>
                    <td colspan="3"><small><em>
                                <font size="1px">*Saya Menyetujui BPJS Kesehatan menggunakan informasi
                                    Medis
                                    Pasien jika diperlukan.</font>
                            </em></small></td>
                    <tr>
                        <td colspan="3">
                            <small><em>
                                    <font size="1px">**SEP bukan sebagai bukti penjaminan peserta</font>
                                </em></small>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="0">
                            <small><em>
                                    <font size="1px">Cetakan ke 1</font>
                                </em></small>
                        </td>
                        <td></td>
                        <td>
                            <small><em>
                                    <font size="1px"><?php echo date("d/m/Y h:i:s A");?></font>
                                </em></small>
                        </td>
                        <td colspan="3">
                            <center>----------------------------------------------------</center>
                        </td>
                    </tr>
                </tbody>
            </table>

        </tbody>
    </table>

    <script>
	window.load = window.print();
  </script>	

</body>

</html>

<?php } ?>