<?php
include ('config.php');
if(!empty($_POST['no_rkm_medis'])){
    $data = array();

    $datapasien = num_rows(query("SELECT KD_BOOKING FROM booking_registrasi WHERE tanggal_periksa = '$_POST[tgl_registrasi]' AND kd_dokter = '$_POST[kd_dokter]' AND kd_poli ='$_POST[kd_poli]' AND no_rkm_medis ='$_POST[no_rkm_medis]'"));
    if($datapasien == 0){
    
        $tanggal=$_POST['tgl_registrasi'];
        $tentukan_hari=date('D',strtotime($tanggal));
        $day = array(
        'Sun' => 'MINGGU',
        'Mon' => 'SENIN',
        'Tue' => 'SELASA',
        'Wed' => 'RABU',
        'Thu' => 'KAMIS',
        'Fri' => 'JUMAT',
        'Sat' => 'SABTU'
        );
        $hari=$day[$tentukan_hari];
                
        $kuota = fetch_array(query("SELECT kuota FROM jadwal WHERE kd_dokter='$_POST[kd_dokter]' and kd_poli='$_POST[kd_poli]' and hari_kerja LIKE '%$hari%'"));

        $jumlah_pasien = fetch_array(query("SELECT
                    COUNT(*) as total_pasien
                    FROM
                    (
                            SELECT no_rkm_medis
                            FROM booking_registrasi
                            where kd_dokter= '$_POST[kd_dokter]'
                            and kd_poli = '{$_POST['kd_poli']}'
                            and tanggal_periksa= '$_POST[tgl_registrasi]'
                            and `status`= 'Belum'
                            UNION ALL
                            SELECT no_rkm_medis
                            FROM reg_periksa
                            where kd_dokter= '$_POST[kd_dokter]'
                            and kd_poli = '{$_POST['kd_poli']}'
                            and tgl_registrasi= '$_POST[tgl_registrasi]'
                    )As total"));
        if ($jumlah_pasien[0] >= $kuota[0]) {
            $data['status'] = 'null';
            $data['result'] = 'Kuota dokter sudah penuh pada tanggal tersebut silahkan pilih tanggal lain';
        }else {
             // get data pasien
             $get_pasien = fetch_array(query("SELECT * FROM pasien WHERE no_rkm_medis = '{$_POST['no_rkm_medis']}'"));
             // set format tanggal
             $tgl_reg = date('Y/m/d', strtotime($_POST['tgl_registrasi']));
             //mencari no rawat terakhir
             $no_rawat_akhir = fetch_array(query("SELECT max(no_rawat) FROM reg_periksa WHERE tgl_registrasi='$_POST[tgl_registrasi]'"));
             $no_urut_rawat = substr($no_rawat_akhir[0], 11, 6);
             $no_rawat = $tgl_reg.'/'.sprintf('%06s', ($no_urut_rawat + 1));
             //mencari no reg terakhir
             $no_reg_akhir = fetch_array(query("SELECT max(no_reg) FROM booking_registrasi WHERE kd_dokter='$_POST[kd_dokter]' and tanggal_periksa='$_POST[tgl_registrasi]'"));
             $no_urut_reg = substr($no_reg_akhir[0], 0, 3);
             $no_reg = sprintf('%03s', ($no_urut_reg + 1));
             // get biaya
             $biaya_reg=fetch_array(query("SELECT registrasilama FROM poliklinik WHERE kd_poli='{$_POST['kd_poli']}'"));
             //menentukan umur sekarang
             list($cY, $cm, $cd) = explode('-', date('Y-m-d'));
             list($Y, $m, $d) = explode('-', date('Y-m-d', strtotime($get_pasien['tgl_lahir'])));
             $umurdaftar = $cY - $Y;
 
             //mencari no reg terakhir
             $bk = fetch_array(query("SELECT max(kd_booking) as kd_booking FROM booking_registrasi"));
             $substr_kdbooking = substr($bk[0],2,5);
             $kd_booking = "BK". sprintf('%05s', ($substr_kdbooking + 1));
 
             $jam = fetch_array(query("SELECT jam_mulai FROM jadwal WHERE kd_dokter='{$_POST['kd_dokter']}' and kd_poli='{$_POST['kd_poli']}' AND hari_kerja LIKE '%$hari%' GROUP BY jam_mulai"));
             $jam_mulai = $jam[0];
             
 
             $no_reg_akhir = fetch_array(query("SELECT max(no_reg) FROM booking_registrasi WHERE kd_dokter='$_POST[kd_dokter]' and tanggal_periksa='$_POST[tgl_registrasi]'"));
             $no_urut_reg = substr($no_reg_akhir[0], 0, 3);
             $no_reg = sprintf('%03s', ($no_urut_reg + 1));
 
             $insert = query("
                     INSERT INTO booking_registrasi
                     SET kd_booking          = '$kd_booking',
                         tanggal_booking     = '$date',
                         jam_booking         = '$time',
                         no_rkm_medis        = '{$_POST['no_rkm_medis']}',
                         tanggal_periksa     = '$tgl_reg',
                         jam_mulai_poli      = '$jam_mulai',
                         kd_dokter           = '{$_POST['kd_dokter']}',
                         kd_poli             = '{$_POST['kd_poli']}',
                         no_reg              = '$no_reg',
                         kd_pj               = '{$_POST['cara_bayar']}',
                         limit_reg           = '0',
                         waktu_kunjungan     = '$jam_mulai',
                         status              = 'belum'
                     ");
 
             $query = $db->query(
             "SELECT
                 a.kd_booking,
                 a.tanggal_periksa,
                 a.jam_booking,
                 a.no_reg,
                 a.waktu_kunjungan,
                 a.jam_mulai_poli,
                 a.no_rkm_medis,
                 b.nm_poli,
                 c.nm_dokter,
                 d.png_jawab,
                 f.nm_pasien
             FROM booking_registrasi a
             LEFT JOIN poliklinik b ON a.kd_poli = b.kd_poli 
             LEFT JOIN dokter c ON a.kd_dokter = c.kd_dokter
             LEFT JOIN pasien f ON a.no_rkm_medis = f.no_rkm_medis
             LEFT JOIN penjab d ON a.kd_pj = d.kd_pj
             WHERE a.kd_booking = '{$kd_booking}'
             ");
 
             if($query->num_rows > 0){
                 $userData = $query->fetch_assoc();
                 $data['status'] = 'ok';
                 $data['result'] = $userData;
             }else{
                 $data['status'] = 'err';
                 $data['result'] = 'Gagal silahkan coba lagi';
             }
        }
}else{
        $data['status'] = 'exist';
        $data['result'] = 'Anda sudah terdaftar pada tanggal tersebut';
    }
}
    //returns data as JSON format
    echo json_encode($data);
?>