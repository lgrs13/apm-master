<?php
include ('config.php');
if (!empty($_POST['noRujukan'])) {
    $polirs = fetch_assoc(query("SELECT kd_poli_rs from maping_poli_bpjs WHERE kd_poli_bpjs='$_POST[tujuan]'"));
    $dokterrs = fetch_assoc(query("SELECT kd_dokter from maping_dokter_dpjpvclaim WHERE kd_dokter_bpjs='$_POST[kodeDPJP]'"));
    
    $kuota = fetch_array(query("SELECT kuota FROM jadwal WHERE kd_dokter='$dokterrs[kd_dokter]' and kd_poli= '$polirs[kd_poli_rs]' and hari_kerja LIKE '%$namahari%'"));
    $jumlah_pasien = fetch_array(query("SELECT
                    COUNT(*) as total_pasien
                    FROM
                    (
                            SELECT no_rkm_medis
                            FROM booking_registrasi
                            where kd_dokter= '$dokterrs[kd_dokter]'
                            and kd_poli = '{$polirs['kd_poli_rs']}'
                            and tanggal_periksa= '$date'
                            and `status`= 'Belum'
                            UNION ALL
                            SELECT no_rkm_medis
                            FROM reg_periksa
                            where kd_dokter= '$dokterrs[kd_dokter]'
                            and kd_poli = '{$polirs['kd_poli_rs']}'
                            and tgl_registrasi= '$date'
                    )
                    As total"));
    //cek kuota dokter
    if ($jumlah_pasien[0] >= $kuota[0]) {
        $databooking = num_rows(query("SELECT kd_booking from booking_registrasi where no_rkm_medis ='$_POST[noMR]' AND kd_poli = '$polirs[kd_poli_rs]' AND kd_dokter = '$dokterrs[kd_dokter]' AND tanggal_periksa = '$date'"));
        if ($databooking == 1) {
            $data = postRujukan();
        }else{
            $data['status'] = 'err';
            $data['result'] = "pendaftaran gagal, kuota dokter penuh, silahkan hubungi petugas";
        }
    }else{
        $data = postRujukan();
    }        
    echo json_encode($data);
    // echo $data;
}

function postRujukan(){
    global $date,$second,$tanggal,$hour;
    date_default_timezone_set('Asia/Jakarta');
    $datapasien = num_rows(query("SELECT
        a.no_rawat 
    FROM
        reg_periksa a
        INNER JOIN maping_poli_bpjs b ON a.kd_poli = b.kd_poli_rs
        INNER JOIN maping_dokter_dpjpvclaim c ON a.kd_dokter = c.kd_dokter 
    WHERE
        a.no_rkm_medis = '$_POST[noMR]'
        AND a.tgl_registrasi = '$date' 
        AND b.kd_poli_bpjs = '$_POST[tujuan]' 
        AND c.kd_dokter = '$_POST[kodeDPJP]'"));
    $cek_norawat = fetch_array(query("select ifnull(MAX(CONVERT(RIGHT(no_rawat,6),signed)),0) as norawat from reg_periksa where tgl_registrasi='$date'"));
    $norawat = str_replace("-","/",$date)."/".str_pad($cek_norawat['norawat'] +1, 6, "0", STR_PAD_LEFT);
    $noSurat = $tanggal.$hour.$second +1;
    if ($noSurat>6) {
        $noSurat = substr($noSurat, 1);
    }
    $notlp = $_POST['noTelp'];
    if (strlen($notlp)>14) {
        $notlp = '00000000000';
    }
    
    if ($datapasien == 0) { 
        $getAPI = fetch_array(query("select * from data_webservice where ID = 'BPJS'"));
        $url = $getAPI['base_url'].'/SEP/1.1/insert';
        $getPPK = fetch_array(query("select kode_ppk from setting"));

        date_default_timezone_set('UTC');
        $tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        $signature = hash_hmac('sha256', $getAPI['user_key']."&".$tStamp, $getAPI['secret_key'], true);
        $encodedSignature = base64_encode($signature);

        $uptade = (query("update data_webservice set token='$encodedSignature' where ID = 'BPJS'"));

        $lastVisit = fetch_assoc(query("SELECT *
        FROM reg_periksa 
        WHERE tgl_registrasi >= DATE_ADD(CURDATE(),INTERVAL -90 DAY)
        AND no_rkm_medis = '$_POST[noMR]'
        ORDER BY tgl_registrasi DESC"));

        $get_dokter = fetch_assoc(query("select kd_dokter_bpjs FROM maping_dokter_dpjpvclaim WHERE kd_dokter = '$lastVisit[kd_dokter]'"));
        if ($get_dokter['kd_dokter_bpjs'] != null)  {
            if($get_dokter['kd_dokter_bpjs'] == 790801){
                $dpjp = 223272;
            }else {
                $dpjp = $get_dokter['kd_dokter_bpjs'];
            }
        }else {
            $dpjp = $_POST['dokterAwal'];
        }
        if($_POST['tujuan']=='IRMF'){
            $tujuan = 'IRM';
        }else{
            $tujuan = $_POST['tujuan'];
        }

        // echo "X-cons-id: " .$getAPI['user_key'];
        // echo "X-timestamp:" .$tStamp;
        // echo "X-signature: " .$encodedSignature;

        $headers = array();
        $headers[] = "X-cons-id: " .$getAPI['user_key'];
        $headers[] = "X-timestamp:" .$tStamp;
        $headers[] = "X-signature: " .$encodedSignature;
        $headers[] = "Content-Type: Application/x-www-form-urlencoded";
        
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d');
        $arr = array(
            "request" => array(
                "t_sep"=> array(
                    "noKartu" => $_POST['noKartu'],
                    "tglSep" => $date,
                    "ppkPelayanan"=> $getPPK['kode_ppk'],
                    "jnsPelayanan"=> $_POST['jnsPelayanan'],
                    "klsRawat"=> '3',
                    "noMR"=> $_POST['noMR'],
                    "rujukan" =>array(
                        "asalRujukan"=> '1',
                        "tglRujukan"=> $_POST['tglRujukan'],
                        "noRujukan"=> $_POST['noRujukan'],
                        "ppkRujukan"=> $_POST['ppkRujukan'],
                    ),
                    "catatan"=> '',
                    "diagAwal"=> $_POST['diagAwal'],
                    "poli"=> array(
                        "tujuan"=> $tujuan,
                        "eksekutif"=> '0',
                    ),
                    "cob"=> array(
                        "cob"=> '0',
                    ),
                    "katarak"=> array(
                        "katarak" => '0',
                    ),
                    "jaminan"=> array(
                        "lakaLantas"=> '0',
                        "penjamin"=> array(
                            "penjamin"=> '1',
                            "tglKejadian"=> '',
                            "keterangan"=> '',
                            "suplesi"=> array(
                                "suplesi" => '0',
                                "noSepSuplesi" => '',
                                "lokasiLaka" => array(
                                    "kdPropinsi" => '',
                                    "kdKabupaten" => '',
                                    "kdKecamatan" => '',
                                )
                            )
                        )
                    ),
                    "skdp"=> array(
                        "noSurat" => "$noSurat",
                        "kodeDPJP" => "$dpjp",
                    ),
                    "noTelp" => $notlp,
                    "user" => 'Anjungan Mandiri RSUD TANAH ABANG',
                ),
            ),
        );

           $arr2= array(
                "metaData" => array(
                   "code"=> "200",
                   "message"=> "Sukses"
                ),
                "response"=> array(
                   "sep"=> array(
                      "catatan"=> "test",
                      "diagnosa"=> "A00.1 - Cholera due to Vibrio cholerae 01, biovar eltor",
                      "jnsPelayanan"=> "R.Inap",
                      "kelasRawat"=> "1",
                      "noSep"=> "0301TY018987V000008",
                      "penjamin"=> "-",
                      "peserta"=> array(
                         "asuransi"=> "-",
                         "hakKelas"=> "Kelas 1",
                         "jnsPeserta"=> "PNS PUSAT",
                         "kelamin"=> "Laki-Laki",
                         "nama"=> "dia",
                         "noKartu"=> "0001112230666",
                         "noMr"=> "123456",
                         "tglLahir"=> "2008-02-05"
                      ),
                      "informasi"=> array(
                         "Dinsos"=>null,
                         "prolanisPRB"=>null,
                         "noSKTM"=>null
                      ),
                      "poli"=> "-",
                      "poliEksekutif"=> "",
                      "tglSep"=> "2017-10-12"
                    ),
                ),
            );

        // $cek_norawat = fetch_array(query("select ifnull(MAX(CONVERT(RIGHT(no_rawat,6),signed)),0) as norawat from reg_periksa where tgl_registrasi='$date'"));
        // $norawat = str_replace("-","/",$date)."/".str_pad($cek_norawat['norawat'] +1, 6, "0", STR_PAD_LEFT);
        // $noMr = $_POST['noMR'];
        // $poli = $_POST['tujuan'];
        // $dokter = $_POST['kodeDPJP'];
        // insert_sep($respon,$norawat,$poli,$dokter,$noSurat);
        // $json = json_encode($arr2);
        // $respon = json_decode($json, true);
        // $data['status'] = 'ok';
        // $data['result'] = $respon;


        $ch = curl_init();
        $json = json_encode($arr);
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);   
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);
        curl_close($ch);

        $respon = json_decode($content, true);
             if ($respon['metaData']['code']==200){ 
            $today = date("Y-m-d");
            $noMr = $_POST['noMR'];;
            $poli = $_POST['tujuan'];
            // dokter yang dipilih
            $dokter = $_POST['kodeDPJP'];
            $save_regis = regis_user($noMr,$poli,$dokter,$norawat);
            if ($save_regis) {
                $save_sep = insert_sep($respon,$norawat,$poli,$dokter,$noSurat);
                if($save_sep){
                    $data['status'] = 'ok';
                    $data['result'] = $respon;
                    $data['no_rawat'] = $norawat;
                }else{
                    $data['status'] = 'err';
                    $data['result'] = "pendaftaran gagal, tidak dapat regis ke local, silahkan hubungi petugas";
                }
            }else{
                //muncul error saat tidak bisa daftar ke simrs     
                $data['status'] = 'err';
                $data['result'] = "pendaftaran gagal, silahkan hubungi petugas";
            }
        }else {
            $data['status'] = 'err';
            $data['result'] = $respon['metaData']['message'];
        }
    }else{
        //cek apakah sudah terdaftar di simrs
        $data['status'] = 'err';
        $data['result'] = "pendaftaran gagal, anda sudah terdaftar pada tanggal tersebut, silahkan hubungi petugas";
    }
    return $data;
}

function regis_user($nomer,$poli,$dokter,$norawat){
    // date_default_timezone_set('Asia/Jakarta');
    // $today = date("Y-m-d");
    // $time = date("H:i:s");
    global $date, $time;
    // echo $uptade;
    $polirs = fetch_assoc(query("SELECT kd_poli_rs from maping_poli_bpjs WHERE kd_poli_bpjs='$poli'"));
    $dokterrs = fetch_assoc(query("SELECT kd_dokter from maping_dokter_dpjpvclaim WHERE kd_dokter_bpjs='$dokter'"));
    $status = fetch_assoc(query("select if((select count(no_rkm_medis) from reg_periksa where no_rkm_medis='$nomer' and kd_poli='$polirs[kd_poli_rs]')>0,'Lama','Baru') as status"));
    // echo $status;
    $cek_noreg = fetch_array(query("select ifnull(MAX(CONVERT(no_reg,signed)),0) as noreg  from reg_periksa where kd_dokter='$dokterrs[kd_dokter]' and tgl_registrasi='$date'"));
    $nreg = $cek_noreg['noreg'] +1;
    $noreg = str_pad($nreg, 3, "0", STR_PAD_LEFT);
    // echo $noreg;
    $get_pasien = fetch_array(query("SELECT * FROM pasien WHERE no_rkm_medis ='$nomer'"));
    list($cY, $cm, $cd) = explode('-', date('Y-m-d'));
    list($Y, $m, $d) = explode('-', date('Y-m-d', strtotime($get_pasien['tgl_lahir'])));
    $umurdaftar = $cY - $Y;
    $biaya_reg=fetch_array(query("SELECT registrasilama FROM poliklinik WHERE kd_poli='$polirs[kd_poli_rs]'"));

    $uptade = (query("UPDATE booking_registrasi SET status='Terdaftar' where kd_poli='$polirs[kd_poli_rs]' AND kd_dokter = '$dokterrs[kd_dokter]' AND tanggal_periksa = '$date' AND no_rkm_medis = '$nomer'"));
    
    // simpan data ke reg periksa
    $insert = query("INSERT
        INTO
        reg_periksa
        SET
        no_reg          = '$noreg',
        no_rawat        = '$norawat',
        tgl_registrasi  = '$date',
        jam_reg         = '$time',
        kd_dokter       = '$dokterrs[kd_dokter]',
        no_rkm_medis    = '$nomer',
        kd_poli         = '$polirs[kd_poli_rs]',
        p_jawab         = '{$get_pasien['namakeluarga']}',
        almt_pj         = '{$get_pasien['alamat']}',
        hubunganpj      = '{$get_pasien['keluarga']}',
        biaya_reg       = '{$biaya_reg['0']}',
        stts            = 'Belum',
        stts_daftar     = 'Lama',
        status_lanjut   = 'Ralan',
        kd_pj           = 'A65',
        umurdaftar      = '{$umurdaftar}',
        sttsumur        = 'Th',
        status_bayar    = 'Belum Bayar',
        status_poli     = '{$status['status']}'
    ");
    return $insert;
}
function insert_sep($data,$norawat,$poli,$dokter,$noSurat){
    date_default_timezone_set('Asia/Jakarta');
    $today = date("Y-m-d");
    $jkel= ($data['response']['sep']['peserta']['kelamin'] == 'Laki-Laki') ? 'L':'P';
    if($poli=='IRMF'){
        $poli = 'IRM';
    }
    if($dokter=='790801'){
        $dokter = '223272';
    }
    $polirs = fetch_assoc(query("SELECT nm_poli FROM poliklinik a INNER JOIN maping_poli_bpjs b ON a.kd_poli = b.kd_poli_rs WHERE b.kd_poli_bpjs = '$poli'"));
    $polibpjs = fetch_assoc(query("SELECT nm_poli_bpjs from maping_poli_bpjs WHERE kd_poli_bpjs='$poli'"));
    $dokterrs = fetch_assoc(query("SELECT * from maping_dokter_dpjpvclaim WHERE kd_dokter_bpjs='$dokter'"));
    
    $insert_sep = query("INSERT
    INTO
    bridging_sep
    SET
    no_sep          = '{$data['response']['sep']['noSep']}',
    no_rawat        = '$norawat',
    tglsep          = '{$data['response']['sep']['tglSep']}',
    tglrujukan      = '{$_POST['tglRujukan']}',
    no_rujukan      = '$_POST[noRujukan]',
    kdppkrujukan    = '{$_POST['ppkRujukan']}',
    nmppkrujukan    = '{$_POST['nmppkrujukan']}',
    kdppkpelayanan  = '0114R053',
    nmppkpelayanan  = 'RSUD TANAH ABANG',
    jnspelayanan    = '2',
    catatan         = '{$data['response']['sep']['catatan']}',
    diagawal        = '{$_POST['diagAwal']}',
    nmdiagnosaawal  = '{$data['response']['sep']['diagnosa']}',
    kdpolitujuan    = '$poli',
    nmpolitujuan    = '$polibpjs[nm_poli_bpjs]',
    klsrawat        = '3',
    lakalantas      = '0',
    user            = 'Anjungan RSUD Tanah Abang',
    nomr            = '{$data['response']['sep']['peserta']['noMr']}',
    nama_pasien     = '{$data['response']['sep']['peserta']['nama']}',
    tanggal_lahir   = '{$data['response']['sep']['peserta']['tglLahir']}',
    peserta         = '{$data['response']['sep']['peserta']['jnsPeserta']}',
    jkel            = '$jkel',
    no_kartu        = '{$data['response']['sep']['peserta']['noKartu']}',
    tglpulang       = '$today',
    asal_rujukan    = '1',
    eksekutif       = '0',
    cob             = '0',
    penjamin        = '-',
    notelep         = '{$_POST['noTelp']}',
    katarak         = '0',
    tglkkl          = '$today',
    keterangankkl   = '-',
    suplesi         = '0',
    no_sep_suplesi  = '-',
    kdprop          = '-',
    nmprop          = '-',
    kdkab           = '-',
    nmkab           = '-',
    kdkec           = '-',
    nmkec           = '-',
    noskdp          = '$noSurat',
    kddpjp          = '$dokter',
    nmdpdjp         = '{$dokterrs['nm_dokter_bpjs']}'
    ");

    return $insert_sep;
}

?>