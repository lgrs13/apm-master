<?php

if (preg_match ('/config.php/', basename($_SERVER['PHP_SELF']))) die ('Unable to access this script directly from browser!');

define ("DB_HOST","192.168.14.106");
define ("DB_USER","fitria");
define ("DB_PASS","");
define ("DB_NAME","sik");
// define ("DB_NAME","db_fitria");
define ("URL","http://localhost/APM-Master");

$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
if($db->connect_error){
    die("Unable to connect database: " . $db->connect_error);
}

function escape($string) {
    global $connection;
    return mysqli_real_escape_string($connection, $string);
}

function query($sql) {
    global $connection;
    $query = mysqli_query($connection, $sql);
    confirm($query);
    return $query;
}

function confirm($query) {
    global $connection;
    if(!$query) {
        die("Query failed! " . mysqli_error($connection));
    }
}

function fetch_array($result) {
    return mysqli_fetch_array($result);
}

function fetch_assoc($result) {
    return mysqli_fetch_assoc($result);
}

function num_rows($result) {
    return mysqli_num_rows($result);
}

function insertTable($table_name, $insertvalue="") {
  $query1 = "";
  $query2 = "";
  if($insertvalue != ""){
    $i=0;
    foreach($insertvalue as $key => $item) {
      if($i == 0) {
        $query1 = $key;
        $query2 = "'".$item."'";
      }
      else{
        $query1 = $query1 . ", ".$key;
        $query2 = $query2 . ", '".$item."'";
      }
      $i++;
    }
  }

  $query = "INSERT INTO ".$table_name." (".$query1.") VALUES (".$query2.")";
  query($query);
}

// Get settings
$getSettings = query("SELECT * FROM setting");
$dataSettings = fetch_assoc($getSettings);

// Get date and time
date_default_timezone_set('Asia/Jakarta');
$tanggal    = date('d');
$bulan      = date('m');
$tahun      = date('Y');
$month      = date('Y-m');
$date       = date('Y-m-d');
$time       = date('H:i:s');
$date_time  = date('Y-m-d H:i:s');
$second     = date("s");
$hour       = date("H");

// Namahari
$hari=fetch_array(query("SELECT DAYNAME(current_date())"));
$namahari="";
if($hari[0]=="Sunday"){
    $namahari="AKHAD";
}else if($hari[0]=="Monday"){
    $namahari="SENIN";
}else if($hari[0]=="Tuesday"){
   	$namahari="SELASA";
}else if($hari[0]=="Wednesday"){
    $namahari="RABU";
}else if($hari[0]=="Thursday"){
    $namahari="KAMIS";
}else if($hari[0]=="Friday"){
    $namahari="JUMAT";
}else if($hari[0]=="Saturday"){
    $namahari="SABTU";
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

header('Content-Type: text/html; charset=utf-8');

?>
