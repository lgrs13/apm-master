<?php
include ('config.php');
if(!empty($_POST['no_rkm_medis_daftar'])){
    $nomer = $_POST['no_rkm_medis_daftar'];
    $data = array();
    $cek = substr($_POST['no_rkm_medis_daftar'],0,2);
    if ($cek == 'BK') {
        $query = $db->query("SELECT a.kd_booking, 
        a.no_rkm_medis,
        a.kd_poli,
        a.kd_dokter,
        b.nm_pasien,
        c.nm_poli,
        d.nm_dokter
        FROM booking_registrasi a
        INNER JOIN pasien b ON a.no_rkm_medis = b.no_rkm_medis
        INNER JOIN poliklinik c on a.kd_poli = c.kd_poli
        INNER JOIN dokter d ON a.kd_dokter = d.kd_dokter
        WHERE a.kd_booking = '$nomer'");

        if($query->num_rows > 0){
            $userData = $query->fetch_assoc();
            $data['status'] = 'ok';
            $data['method'] = '1';
            // 1 = booking 2=nonboking
            $data['result'] = $userData;
        //error KD booking   
        }else{
            $data['status'] = 'err';
            $data['result'] = 'pendaftaran Gagal, Kode booking tidak di temukan, silahkan hubungi petugas';
        }
    }else{
        $query = $db->query("SELECT * FROM pasien WHERE no_rkm_medis = '$nomer'");
        if($query->num_rows > 0){
            
            $tanggal = date("Y-m-d");
            $tentukan_hari = date('D',strtotime($tanggal));
            $day = array(
                'Sun' => 'AKHAD',
                'Mon' => 'SENIN',
                'Tue' => 'SELASA',
                'Wed' => 'RABU',
                'Thu' => 'KAMIS',
                'Fri' => 'JUMAT',
                'Sat' => 'SABTU'
            );
            $hari=$day[$tentukan_hari];
            $query2 = $db->query("
                SELECT
                jadwal.kd_poli AS kd_poli,
                poliklinik.nm_poli AS nm_poli,
                DATE_FORMAT(jadwal.jam_mulai, '%H:%i') AS jam_mulai,
                DATE_FORMAT(jadwal.jam_selesai, '%H:%i') AS jam_selesai
                FROM
                jadwal,
                poliklinik,
                dokter
                WHERE
                jadwal.kd_poli = poliklinik.kd_poli
                AND
                jadwal.kd_dokter = dokter.kd_dokter
                AND
                hari_kerja LIKE '%$hari%'
                GROUP BY
                poliklinik.kd_poli
            ");
            $userData = $query->fetch_assoc();
            $data['status'] = 'ok';
            $data['method'] = '2';
            // 1 = booking 2=nonboking
            $data['pasien'] = $userData;
            while ($poli = $query2->fetch_assoc()) {
                $data['poli'][] = $poli;
            }
            

        }else{
            $data['status'] = 'err';
            $data['result'] = 'pasien tidak ditemukan, Silahkan hubungi petugas';
        }

    }
        //returns data as JSON format
        echo json_encode($data);
}
?>