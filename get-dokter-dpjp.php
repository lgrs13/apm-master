<?php
include ('config.php');
if(!empty($_POST['kd_poli_bpjs'])){
    $data = array();

      $poliBPJS = $_POST['kd_poli_bpjs'];
      $tanggal=date("Y-m-d");
      $tentukan_hari = date('D',strtotime($tanggal));
      $day = array(
        'Sun' => 'AKHAD',
        'Mon' => 'SENIN',
        'Tue' => 'SELASA',
        'Wed' => 'RABU',
        'Thu' => 'KAMIS',
        'Fri' => 'JUMAT',
        'Sat' => 'SABTU'
      );
      $hari=$day[$tentukan_hari];

      //get data from the database
      $query = $db->query("SELECT
      a.kd_dokter,
      a.kd_poli,
      b.kd_dokter_bpjs,
      c.kd_poli_bpjs,
      d.nm_dokter
      FROM
      jadwal a
      INNER JOIN maping_dokter_dpjpvclaim b on a.kd_dokter = b.kd_dokter
      INNER JOIN maping_poli_bpjs c on a.kd_poli = c.kd_poli_rs
      INNER JOIN dokter d on a.kd_dokter = d.kd_dokter
      AND c.kd_poli_bpjs = '$poliBPJS'
      AND a.hari_kerja = '$hari'");

      if($query->num_rows > 0){
          while ($userData = $query->fetch_assoc()) {
            $data['status'] = 'ok';
            $data['result'][] = $userData;
          }
      }else{
          $data['status'] = 'err';
          $data['result'] = '';
      }

    //returns data as JSON format
    echo json_encode($data);

}
?>
