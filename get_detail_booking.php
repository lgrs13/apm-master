<?php
include ('config.php');
require __DIR__ . '/plugins/escpos/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

if(!empty($_POST['kode_booking'])){

    $post_kd_booking=$_POST['kode_booking'];

    $reg_det = fetch_array(query("
    SELECT
        a.kd_booking,
        a.tanggal_periksa,
        a.jam_booking,
        a.no_reg,
        a.waktu_kunjungan,
        a.jam_mulai_poli,
        b.nm_poli,
        c.nm_dokter,
        d.png_jawab,
        f.nm_pasien
    FROM booking_registrasi a
    LEFT JOIN poliklinik b ON a.kd_poli = b.kd_poli
    LEFT JOIN dokter c ON a.kd_dokter = c.kd_dokter
    LEFT JOIN pasien f ON a.no_rkm_medis = f.no_rkm_medis
    LEFT JOIN penjab d ON a.kd_pj = d.kd_pj
    WHERE a.kd_booking = '$post_kd_booking'
    "));

    try {
        // $ipaddres = get_client_ip();
        $bookcode = $reg_det['kd_booking'];
        // Enter the share name for your USB printer here
        // $connector = null;
        $connector=new WindowsPrintConnector("THERMAL");
        // $connector = new WindowsPrintConnector("smb://Administrator@".get_client_ip()."/THERMAL");
        // $connector = new WindowsPrintConnector("smb://sisan@192.168.11.8/THERMAL");
        /* Print a "Hello world" receipt" */
        $printer=new Printer($connector);

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        $printer->text("RUMAH SAKIT UMUM DAERAH \n TANAH ABANG \n");
        $printer->selectPrintMode(printer::MODE_FONT_A);
        $printer->text("ANJUNGAN PENDAFTARAN MANDIRI\n");
            
        $printer->feed();
        $printer->feed();   

        // $printer->setEmphasis(true);
        // $printer->text($bookcode);
        // $printer->setEmphasis(false);
        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->text("Kode Booking    : ".$reg_det['kd_booking']."\n");
        $printer->text("Nama            : ".$reg_det['nm_pasien']."\n");
        $printer->text("Tanggal Periksa : ".$reg_det['tanggal_periksa']."\n");
        $printer->text("Poliklinik      : ".$reg_det['nm_poli']."\n");
        $printer->text("Dokter          : ".$reg_det['nm_dokter']); 

        $printer->feed();
        $printer->feed();

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_ABOVE);
        $printer->barcode($bookcode, Printer::BARCODE_CODE39);
        $printer->feed();
        $printer->selectPrintMode(Printer::MODE_EMPHASIZED);

        if(substr($reg_det['jam_mulai_poli'],0,2) < 12){
            $printer->text("Datanglah sebelum jam 9 pagi, jika Anda tidak hadir pada jam tersebut Anda di anggap tidak datang dan kode booking ini tidak berlaku lagi");
        }else{
            $printer->text("Datanglah sebelum jam 15 sore, jika Anda tidak hadir pada jam tersebut Anda di anggap tidak datang dan kode booking ini tidak berlaku lagi");
        }

        $printer->feed();
        $printer->text("Teima kasih atas kepercayaan Anda");
        $printer->text("Tunjukan bukti ini ke petugas pendaftaran di RSUD tanah abang");

        $printer->feed();
        $printer->feed();
        $printer->feed();
        $printer->cut();
        $printer->close();  
    }

    catch (Exception $e) {
        $bookcode = "";
    }
    
    if($bookcode != ""){
        $data['status'] = 'ok';
    }else{
        $data['status'] = 'err';
    }
    //returns data as JSON format
    echo json_encode($data);
}
?>
