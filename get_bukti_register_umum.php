<?php
include ('config.php');
require __DIR__ . '/plugins/escpos/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

    if(!empty($_POST['no_rawat'])){
    $no_rawat = $_POST['no_rawat'];

    $reg_det = fetch_array(query("
        SELECT
        a.no_reg,
        a.no_rkm_medis,
        a.tgl_registrasi,
        b.png_jawab,
        c.nm_poli,
        d.nm_dokter,
        e.nm_pasien,
        e.tgl_lahir,
        e.umur
    FROM reg_periksa a
    INNER JOIN penjab b ON a.kd_pj = b.kd_pj
    INNER JOIN poliklinik c on a.kd_poli = c.kd_poli
    INNER JOIN dokter d ON a.kd_dokter = d.kd_dokter
    INNER JOIN pasien e on a.no_rkm_medis = e.no_rkm_medis
    WHERE no_rawat = '{$_POST['no_rawat']}'
    "));


    try {
        // Enter the share name for your USB printer here
        // $connector = null;
        $connector=new WindowsPrintConnector("THERMAL");
        // $connector = new WindowsPrintConnector("smb://sisan@192.168.11.8/THERMAL");
        // $connector = new WindowsPrintConnector("smb://".'192.168.14.80'."/".'epson');
        /* Print a "Hello world" receipt" */
        $printer=new Printer($connector);

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        $printer->text("RUMAH SAKIT UMUM DAERAH \n TANAH ABANG \n");
        $printer->selectPrintMode(printer::MODE_FONT_A);
        $printer->text("BUKTI REGISTER PENDAFTARAN\n");
        $printer->feed();
        $printer -> setTextSize(4, 5);
        $printer -> text($reg_det['no_reg']."\n");
        $printer -> setTextSize(2, 2);
        $printer -> text($reg_det['nm_poli']."\n");
        $printer -> setTextSize(1, 1);
        $printer -> text($reg_det['nm_dokter']."\n");
        $printer -> setTextSize(2, 2);
        $printer -> text($reg_det['png_jawab']."\n");

        $printer->feed();   

        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->setFont(Printer::FONT_A);
        $printer -> setTextSize(1, 1);
        $printer->text("Nomer Rawat         : ".$_POST['no_rawat']."\n");
        $printer->text("No. Rekam medis     : ".$reg_det['no_rkm_medis']."\n");
        $printer->text("Nama                : ".$reg_det['nm_pasien']."\n");
        $printer->text("umur                : ".$reg_det['umur']."\n");
        $printer->text("Tanggal Registrasi  : ".$reg_det['tgl_registrasi']."\n");

        $printer->feed();
        $printer->feed();

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_ABOVE);
        $printer->barcode($no_rawat, Printer::BARCODE_CODE39);
        $printer->feed();
        $printer->selectPrintMode(Printer::MODE_EMPHASIZED);

        $printer->text("Teima kasih atas kepercayaan Anda");

        $printer->feed();
        $printer->feed();
        $printer->feed();
        $printer->cut();
        $printer->close();
        $bookcode = '-';
    }

    catch (Exception $e) {
        $bookcode = "";
    }
    if($bookcode != ""){
        printrm($reg_det);
        $data['status'] = 'ok';
        $data['result'] = '-';
    }else{
        $data['status'] = 'err';
        $data['result'] = 'Gagagl Print, silahkan hubungi petugas';
    }
    //returns data as JSON format
    echo json_encode($data);
    
}

function printrm($reg_det){

    try{
    // Enter the share name for your printer here, as a smb:// url format
        $connector = new WindowsPrintConnector("smb://guest@192.168.13.2/RM LQ 310");
        //$connector = new WindowsPrintConnector("smb://Guest@computername/Receipt Printer");
        //$connector = new WindowsPrintConnector("smb://FooUser:secret@computername/workgroup/Receipt Printer");
        //$connector = new WindowsPrintConnector("smb://User:secret@computername/Receipt Printer");
        
        /* Print a "Hello world" receipt" */
        $printer = new Printer($connector);
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
        $printer->text("RUMAH SAKIT UMUM DAERAH \n TANAH ABANG \n");
        $printer->selectPrintMode(printer::MODE_FONT_A);            $printer->text("BUKTI REGISTER PENDAFTARAN\n");
        $printer->feed();
        $printer -> text($reg_det['no_reg']."\n");
        $printer -> setTextSize(2, 2);
        $printer -> text($reg_det['nm_poli']."\n");
        $printer -> setTextSize(1, 1);
        $printer -> text($reg_det['nm_dokter']."\n");
        $printer -> setTextSize(2, 2);
        $printer -> text($reg_det['png_jawab']."\n");

        $printer->feed();   

        $printer -> setTextSize(1, 1);
        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->setFont(Printer::FONT_A);
        $printer->text("Nomer Rawat         : ".$_POST['no_rawat']."\n");
        $printer->text("No. Rekam medis     : ".$reg_det['no_rkm_medis']."\n");
        $printer->text("Nama                : ".$reg_det['nm_pasien']."\n");
        $printer->text("umur                : ".$reg_det['umur']."\n");
        $printer->text("Tanggal Registrasi  : ".$reg_det['tgl_registrasi']."\n");

        // $printer -> cut();
        
        /* Close printer */
        $printer -> close();
    } catch (Exception $e) {
        echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
    }
}
?>
